﻿Public Class Novedades
    Private Sub Novedades_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If My.Settings.Language = "es" Then
            Try
                RichTextBox1.Text = My.Computer.FileSystem.ReadAllText("changelog-es.vinsoft")
            Catch ex As Exception
                MessageBox.Show("El archivo que contiene el listado de cambios no pudo ser encontrado.", "Bashing - Vinari Software", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Me.Close()
            End Try
            Label1.Text = "Novedades en Bashing " & Application.ProductVersion

        ElseIf My.Settings.Language = "en" Then
            Try
                RichTextBox1.Text = My.Computer.FileSystem.ReadAllText("changelog-en.vinsoft")
            Catch ex As Exception
                MessageBox.Show("The file that contains the changelog could not be found.", "Bashing - Vinari Software", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Me.Close()
            End Try
            Label1.Text = "What's new in Bashing " & Application.ProductVersion
            Button1.Text = "Accept"
            Me.Text = "Bashing - Vinari Software | What's new"
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class