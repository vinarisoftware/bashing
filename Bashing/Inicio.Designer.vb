﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Script
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Script))
        Me.FastColoredTextBox1 = New FastColoredTextBoxNS.FastColoredTextBox()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.NuevaInstanciaContext = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevoDocumentoContext = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripSeparator()
        Me.DeshacerContext = New System.Windows.Forms.ToolStripMenuItem()
        Me.RehacerContext = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator()
        Me.CopiarContext = New System.Windows.Forms.ToolStripMenuItem()
        Me.CortarContext = New System.Windows.Forms.ToolStripMenuItem()
        Me.PegarContext = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem10 = New System.Windows.Forms.ToolStripSeparator()
        Me.SeleccionarTodoContext = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem11 = New System.Windows.Forms.ToolStripSeparator()
        Me.BuscarContext = New System.Windows.Forms.ToolStripMenuItem()
        Me.BuscarYReemplazarContext = New System.Windows.Forms.ToolStripMenuItem()
        Me.IrALaLineaContext = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem12 = New System.Windows.Forms.ToolStripSeparator()
        Me.InformacionArchivoContext = New System.Windows.Forms.ToolStripMenuItem()
        Me.ColorDialog1 = New System.Windows.Forms.ColorDialog()
        Me.ArchivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AbrirScriptToolMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.GuardarScriptComoToolMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.GuardarScriptToolMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.NuevaInstanciaToolMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.NuevoDeocumentoToolMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem13 = New System.Windows.Forms.ToolStripSeparator()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeshacerToolMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.RehacerToolMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem5 = New System.Windows.Forms.ToolStripSeparator()
        Me.CopiarToolMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.CortarToolMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.PegarToolMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem21 = New System.Windows.Forms.ToolStripSeparator()
        Me.CortarLineaToolMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.SoloLecturaToolMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.SeleccionarTodoToolMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem8 = New System.Windows.Forms.ToolStripSeparator()
        Me.BuscarToolMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.BuscarYReemplazarToolMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.IrALineaToolMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem16 = New System.Windows.Forms.ToolStripSeparator()
        Me.PreferenciasToolMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.VerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ColorMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ColorDeLaFuenteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem7 = New System.Windows.Forms.ToolStripSeparator()
        Me.DraculaTheme = New System.Windows.Forms.ToolStripMenuItem()
        Me.SolarOscuroTheme = New System.Windows.Forms.ToolStripMenuItem()
        Me.SolarClaroTheme = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClassicTheme = New System.Windows.Forms.ToolStripMenuItem()
        Me.MoonTheme = New System.Windows.Forms.ToolStripMenuItem()
        Me.WarmCoffeeTheme = New System.Windows.Forms.ToolStripMenuItem()
        Me.KingdomTheme = New System.Windows.Forms.ToolStripMenuItem()
        Me.AuroraTheme = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalmOceanTheme = New System.Windows.Forms.ToolStripMenuItem()
        Me.GrapeTheme = New System.Windows.Forms.ToolStripMenuItem()
        Me.BlackBoardTheme = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem6 = New System.Windows.Forms.ToolStripSeparator()
        Me.CambiarEstiloDeFuenteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AumentarFuenteMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DisminuirFuenteMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AcercaDeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BashingAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem9 = New System.Windows.Forms.ToolStripSeparator()
        Me.VinariSoftwareAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.VinariOSAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
        Me.CambiosEnEstaVersiónDeBashingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip = New System.Windows.Forms.MenuStrip()
        Me.LenguajeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BashLanguage = New System.Windows.Forms.ToolStripMenuItem()
        Me.CommandLanguage = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem18 = New System.Windows.Forms.ToolStripSeparator()
        Me.RubyLanguage = New System.Windows.Forms.ToolStripMenuItem()
        Me.PythonLanguage = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem19 = New System.Windows.Forms.ToolStripSeparator()
        Me.RustLanguage = New System.Windows.Forms.ToolStripMenuItem()
        Me.ValaLanguage = New System.Windows.Forms.ToolStripMenuItem()
        Me.LuaLanguage = New System.Windows.Forms.ToolStripMenuItem()
        Me.JavaLanguage = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem20 = New System.Windows.Forms.ToolStripSeparator()
        Me.CppLanguage = New System.Windows.Forms.ToolStripMenuItem()
        Me.CLanguage = New System.Windows.Forms.ToolStripMenuItem()
        Me.ObjectiveCLanguage = New System.Windows.Forms.ToolStripMenuItem()
        Me.CSharpLanguage = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem17 = New System.Windows.Forms.ToolStripSeparator()
        Me.PHPLanguage = New System.Windows.Forms.ToolStripMenuItem()
        Me.SQLLanguage = New System.Windows.Forms.ToolStripMenuItem()
        Me.JSLanguage = New System.Windows.Forms.ToolStripMenuItem()
        Me.CSSLanguage = New System.Windows.Forms.ToolStripMenuItem()
        Me.HTMLLanguage = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        CType(Me.FastColoredTextBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.MenuStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'FastColoredTextBox1
        '
        Me.FastColoredTextBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FastColoredTextBox1.AutoCompleteBrackets = True
        Me.FastColoredTextBox1.AutoCompleteBracketsList = New Char() {Global.Microsoft.VisualBasic.ChrW(40), Global.Microsoft.VisualBasic.ChrW(41), Global.Microsoft.VisualBasic.ChrW(123), Global.Microsoft.VisualBasic.ChrW(125), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(39)}
        Me.FastColoredTextBox1.AutoIndentCharsPatterns = "^\s*[\w\.]+(\s\w+)?\s*(?<range>=)\s*(?<range>[^;=]+);" & Global.Microsoft.VisualBasic.ChrW(10) & "^\s*(case|default)\s*[^:]*(" &
    "?<range>:)\s*(?<range>[^;]+);"
        Me.FastColoredTextBox1.AutoScrollMinSize = New System.Drawing.Size(25, 15)
        Me.FastColoredTextBox1.BackBrush = Nothing
        Me.FastColoredTextBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.FastColoredTextBox1.BookmarkColor = System.Drawing.Color.LemonChiffon
        Me.FastColoredTextBox1.CaretColor = System.Drawing.Color.White
        Me.FastColoredTextBox1.CharHeight = 15
        Me.FastColoredTextBox1.CharWidth = 7
        Me.FastColoredTextBox1.ContextMenuStrip = Me.ContextMenuStrip1
        Me.FastColoredTextBox1.CurrentLineColor = System.Drawing.Color.FromArgb(CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.FastColoredTextBox1.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.FastColoredTextBox1.DisabledColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(180, Byte), Integer))
        Me.FastColoredTextBox1.FoldingIndicatorColor = System.Drawing.Color.MintCream
        Me.FastColoredTextBox1.Font = New System.Drawing.Font("Consolas", 9.75!)
        Me.FastColoredTextBox1.ForeColor = System.Drawing.Color.White
        Me.FastColoredTextBox1.IndentBackColor = System.Drawing.Color.FromArgb(CType(CType(44, Byte), Integer), CType(CType(40, Byte), Integer), CType(CType(47, Byte), Integer))
        Me.FastColoredTextBox1.IsReplaceMode = False
        Me.FastColoredTextBox1.LeftBracket = Global.Microsoft.VisualBasic.ChrW(123)
        Me.FastColoredTextBox1.LeftBracket2 = Global.Microsoft.VisualBasic.ChrW(91)
        Me.FastColoredTextBox1.LineNumberColor = System.Drawing.Color.FromArgb(CType(CType(139, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(225, Byte), Integer))
        Me.FastColoredTextBox1.Location = New System.Drawing.Point(0, 26)
        Me.FastColoredTextBox1.Name = "FastColoredTextBox1"
        Me.FastColoredTextBox1.Paddings = New System.Windows.Forms.Padding(0)
        Me.FastColoredTextBox1.RightBracket = Global.Microsoft.VisualBasic.ChrW(125)
        Me.FastColoredTextBox1.RightBracket2 = Global.Microsoft.VisualBasic.ChrW(93)
        Me.FastColoredTextBox1.SelectionColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(106, Byte), Integer), CType(CType(90, Byte), Integer), CType(CType(205, Byte), Integer))
        Me.FastColoredTextBox1.ServiceColors = CType(resources.GetObject("FastColoredTextBox1.ServiceColors"), FastColoredTextBoxNS.ServiceColors)
        Me.FastColoredTextBox1.Size = New System.Drawing.Size(974, 587)
        Me.FastColoredTextBox1.TabIndex = 1
        Me.FastColoredTextBox1.Zoom = 100
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Font = New System.Drawing.Font("Lucida Sans Unicode", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevaInstanciaContext, Me.NuevoDocumentoContext, Me.ToolStripMenuItem4, Me.DeshacerContext, Me.RehacerContext, Me.ToolStripMenuItem2, Me.CopiarContext, Me.CortarContext, Me.PegarContext, Me.ToolStripMenuItem10, Me.SeleccionarTodoContext, Me.ToolStripMenuItem11, Me.BuscarContext, Me.BuscarYReemplazarContext, Me.IrALaLineaContext, Me.ToolStripMenuItem12, Me.InformacionArchivoContext})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(304, 298)
        '
        'NuevaInstanciaContext
        '
        Me.NuevaInstanciaContext.Image = CType(resources.GetObject("NuevaInstanciaContext.Image"), System.Drawing.Image)
        Me.NuevaInstanciaContext.Name = "NuevaInstanciaContext"
        Me.NuevaInstanciaContext.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.NuevaInstanciaContext.Size = New System.Drawing.Size(303, 22)
        Me.NuevaInstanciaContext.Text = "Crear nueva instancia de Bashing"
        '
        'NuevoDocumentoContext
        '
        Me.NuevoDocumentoContext.Image = CType(resources.GetObject("NuevoDocumentoContext.Image"), System.Drawing.Image)
        Me.NuevoDocumentoContext.Name = "NuevoDocumentoContext"
        Me.NuevoDocumentoContext.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.NuevoDocumentoContext.Size = New System.Drawing.Size(303, 22)
        Me.NuevoDocumentoContext.Text = "Nuevo documento en blanco"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(300, 6)
        '
        'DeshacerContext
        '
        Me.DeshacerContext.Image = CType(resources.GetObject("DeshacerContext.Image"), System.Drawing.Image)
        Me.DeshacerContext.Name = "DeshacerContext"
        Me.DeshacerContext.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Z), System.Windows.Forms.Keys)
        Me.DeshacerContext.Size = New System.Drawing.Size(303, 22)
        Me.DeshacerContext.Text = "Deshacer"
        '
        'RehacerContext
        '
        Me.RehacerContext.Image = CType(resources.GetObject("RehacerContext.Image"), System.Drawing.Image)
        Me.RehacerContext.Name = "RehacerContext"
        Me.RehacerContext.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Y), System.Windows.Forms.Keys)
        Me.RehacerContext.Size = New System.Drawing.Size(303, 22)
        Me.RehacerContext.Text = "Rehacer"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(300, 6)
        '
        'CopiarContext
        '
        Me.CopiarContext.Image = CType(resources.GetObject("CopiarContext.Image"), System.Drawing.Image)
        Me.CopiarContext.Name = "CopiarContext"
        Me.CopiarContext.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.CopiarContext.Size = New System.Drawing.Size(303, 22)
        Me.CopiarContext.Text = "Copiar"
        '
        'CortarContext
        '
        Me.CortarContext.Image = CType(resources.GetObject("CortarContext.Image"), System.Drawing.Image)
        Me.CortarContext.Name = "CortarContext"
        Me.CortarContext.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.CortarContext.Size = New System.Drawing.Size(303, 22)
        Me.CortarContext.Text = "Cortar"
        '
        'PegarContext
        '
        Me.PegarContext.Image = CType(resources.GetObject("PegarContext.Image"), System.Drawing.Image)
        Me.PegarContext.Name = "PegarContext"
        Me.PegarContext.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.V), System.Windows.Forms.Keys)
        Me.PegarContext.Size = New System.Drawing.Size(303, 22)
        Me.PegarContext.Text = "Pegar"
        '
        'ToolStripMenuItem10
        '
        Me.ToolStripMenuItem10.Name = "ToolStripMenuItem10"
        Me.ToolStripMenuItem10.Size = New System.Drawing.Size(300, 6)
        '
        'SeleccionarTodoContext
        '
        Me.SeleccionarTodoContext.Image = CType(resources.GetObject("SeleccionarTodoContext.Image"), System.Drawing.Image)
        Me.SeleccionarTodoContext.Name = "SeleccionarTodoContext"
        Me.SeleccionarTodoContext.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.SeleccionarTodoContext.Size = New System.Drawing.Size(303, 22)
        Me.SeleccionarTodoContext.Text = "Seleccionar todo"
        '
        'ToolStripMenuItem11
        '
        Me.ToolStripMenuItem11.Name = "ToolStripMenuItem11"
        Me.ToolStripMenuItem11.Size = New System.Drawing.Size(300, 6)
        '
        'BuscarContext
        '
        Me.BuscarContext.Image = CType(resources.GetObject("BuscarContext.Image"), System.Drawing.Image)
        Me.BuscarContext.Name = "BuscarContext"
        Me.BuscarContext.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F), System.Windows.Forms.Keys)
        Me.BuscarContext.Size = New System.Drawing.Size(303, 22)
        Me.BuscarContext.Text = "Buscar"
        '
        'BuscarYReemplazarContext
        '
        Me.BuscarYReemplazarContext.Image = CType(resources.GetObject("BuscarYReemplazarContext.Image"), System.Drawing.Image)
        Me.BuscarYReemplazarContext.Name = "BuscarYReemplazarContext"
        Me.BuscarYReemplazarContext.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.H), System.Windows.Forms.Keys)
        Me.BuscarYReemplazarContext.Size = New System.Drawing.Size(303, 22)
        Me.BuscarYReemplazarContext.Text = "Buscar y reemplazar"
        '
        'IrALaLineaContext
        '
        Me.IrALaLineaContext.Image = CType(resources.GetObject("IrALaLineaContext.Image"), System.Drawing.Image)
        Me.IrALaLineaContext.Name = "IrALaLineaContext"
        Me.IrALaLineaContext.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
        Me.IrALaLineaContext.Size = New System.Drawing.Size(303, 22)
        Me.IrALaLineaContext.Text = "Ir a la linea"
        '
        'ToolStripMenuItem12
        '
        Me.ToolStripMenuItem12.Name = "ToolStripMenuItem12"
        Me.ToolStripMenuItem12.Size = New System.Drawing.Size(300, 6)
        '
        'InformacionArchivoContext
        '
        Me.InformacionArchivoContext.Image = CType(resources.GetObject("InformacionArchivoContext.Image"), System.Drawing.Image)
        Me.InformacionArchivoContext.Name = "InformacionArchivoContext"
        Me.InformacionArchivoContext.ShortcutKeys = System.Windows.Forms.Keys.F10
        Me.InformacionArchivoContext.Size = New System.Drawing.Size(303, 22)
        Me.InformacionArchivoContext.Text = "Obtener información del archivo"
        '
        'ArchivoToolStripMenuItem
        '
        Me.ArchivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AbrirScriptToolMenu, Me.GuardarScriptComoToolMenu, Me.GuardarScriptToolMenu, Me.ToolStripMenuItem1, Me.NuevaInstanciaToolMenu, Me.NuevoDeocumentoToolMenu, Me.ToolStripMenuItem13, Me.SalirToolStripMenuItem})
        Me.ArchivoToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.ArchivoToolStripMenuItem.Name = "ArchivoToolStripMenuItem"
        Me.ArchivoToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.ArchivoToolStripMenuItem.Text = "&Archivo"
        '
        'AbrirScriptToolMenu
        '
        Me.AbrirScriptToolMenu.Image = CType(resources.GetObject("AbrirScriptToolMenu.Image"), System.Drawing.Image)
        Me.AbrirScriptToolMenu.Name = "AbrirScriptToolMenu"
        Me.AbrirScriptToolMenu.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.AbrirScriptToolMenu.Size = New System.Drawing.Size(303, 22)
        Me.AbrirScriptToolMenu.Text = "Abrir Script"
        '
        'GuardarScriptComoToolMenu
        '
        Me.GuardarScriptComoToolMenu.Image = CType(resources.GetObject("GuardarScriptComoToolMenu.Image"), System.Drawing.Image)
        Me.GuardarScriptComoToolMenu.Name = "GuardarScriptComoToolMenu"
        Me.GuardarScriptComoToolMenu.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
            Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.GuardarScriptComoToolMenu.Size = New System.Drawing.Size(303, 22)
        Me.GuardarScriptComoToolMenu.Text = "Guardar Script como"
        '
        'GuardarScriptToolMenu
        '
        Me.GuardarScriptToolMenu.Enabled = False
        Me.GuardarScriptToolMenu.Image = CType(resources.GetObject("GuardarScriptToolMenu.Image"), System.Drawing.Image)
        Me.GuardarScriptToolMenu.Name = "GuardarScriptToolMenu"
        Me.GuardarScriptToolMenu.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.GuardarScriptToolMenu.Size = New System.Drawing.Size(303, 22)
        Me.GuardarScriptToolMenu.Text = "Guardar Script"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(300, 6)
        '
        'NuevaInstanciaToolMenu
        '
        Me.NuevaInstanciaToolMenu.Image = CType(resources.GetObject("NuevaInstanciaToolMenu.Image"), System.Drawing.Image)
        Me.NuevaInstanciaToolMenu.Name = "NuevaInstanciaToolMenu"
        Me.NuevaInstanciaToolMenu.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.NuevaInstanciaToolMenu.Size = New System.Drawing.Size(303, 22)
        Me.NuevaInstanciaToolMenu.Text = "Crear nueva instancia de Bashing"
        '
        'NuevoDeocumentoToolMenu
        '
        Me.NuevoDeocumentoToolMenu.Image = CType(resources.GetObject("NuevoDeocumentoToolMenu.Image"), System.Drawing.Image)
        Me.NuevoDeocumentoToolMenu.Name = "NuevoDeocumentoToolMenu"
        Me.NuevoDeocumentoToolMenu.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.NuevoDeocumentoToolMenu.Size = New System.Drawing.Size(303, 22)
        Me.NuevoDeocumentoToolMenu.Text = "Nuevo documento en blanco"
        '
        'ToolStripMenuItem13
        '
        Me.ToolStripMenuItem13.Name = "ToolStripMenuItem13"
        Me.ToolStripMenuItem13.Size = New System.Drawing.Size(300, 6)
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.Image = CType(resources.GetObject("SalirToolStripMenuItem.Image"), System.Drawing.Image)
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Q), System.Windows.Forms.Keys)
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(303, 22)
        Me.SalirToolStripMenuItem.Text = "Salir"
        '
        'EditarToolStripMenuItem
        '
        Me.EditarToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeshacerToolMenu, Me.RehacerToolMenu, Me.ToolStripMenuItem5, Me.CopiarToolMenu, Me.CortarToolMenu, Me.PegarToolMenu, Me.ToolStripMenuItem21, Me.CortarLineaToolMenu, Me.SoloLecturaToolMenu, Me.SeleccionarTodoToolMenu, Me.ToolStripMenuItem8, Me.BuscarToolMenu, Me.BuscarYReemplazarToolMenu, Me.IrALineaToolMenu, Me.ToolStripMenuItem16, Me.PreferenciasToolMenu})
        Me.EditarToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.EditarToolStripMenuItem.Name = "EditarToolStripMenuItem"
        Me.EditarToolStripMenuItem.Size = New System.Drawing.Size(53, 20)
        Me.EditarToolStripMenuItem.Text = "&Editar"
        '
        'DeshacerToolMenu
        '
        Me.DeshacerToolMenu.Image = CType(resources.GetObject("DeshacerToolMenu.Image"), System.Drawing.Image)
        Me.DeshacerToolMenu.Name = "DeshacerToolMenu"
        Me.DeshacerToolMenu.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Z), System.Windows.Forms.Keys)
        Me.DeshacerToolMenu.Size = New System.Drawing.Size(239, 22)
        Me.DeshacerToolMenu.Text = "Deshacer"
        '
        'RehacerToolMenu
        '
        Me.RehacerToolMenu.Image = CType(resources.GetObject("RehacerToolMenu.Image"), System.Drawing.Image)
        Me.RehacerToolMenu.Name = "RehacerToolMenu"
        Me.RehacerToolMenu.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Y), System.Windows.Forms.Keys)
        Me.RehacerToolMenu.Size = New System.Drawing.Size(239, 22)
        Me.RehacerToolMenu.Text = "Rehacer"
        '
        'ToolStripMenuItem5
        '
        Me.ToolStripMenuItem5.Name = "ToolStripMenuItem5"
        Me.ToolStripMenuItem5.Size = New System.Drawing.Size(236, 6)
        '
        'CopiarToolMenu
        '
        Me.CopiarToolMenu.Image = CType(resources.GetObject("CopiarToolMenu.Image"), System.Drawing.Image)
        Me.CopiarToolMenu.Name = "CopiarToolMenu"
        Me.CopiarToolMenu.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.CopiarToolMenu.Size = New System.Drawing.Size(239, 22)
        Me.CopiarToolMenu.Text = "Copiar"
        '
        'CortarToolMenu
        '
        Me.CortarToolMenu.Image = CType(resources.GetObject("CortarToolMenu.Image"), System.Drawing.Image)
        Me.CortarToolMenu.Name = "CortarToolMenu"
        Me.CortarToolMenu.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.CortarToolMenu.Size = New System.Drawing.Size(239, 22)
        Me.CortarToolMenu.Text = "Cortar"
        '
        'PegarToolMenu
        '
        Me.PegarToolMenu.Image = CType(resources.GetObject("PegarToolMenu.Image"), System.Drawing.Image)
        Me.PegarToolMenu.Name = "PegarToolMenu"
        Me.PegarToolMenu.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.V), System.Windows.Forms.Keys)
        Me.PegarToolMenu.Size = New System.Drawing.Size(239, 22)
        Me.PegarToolMenu.Text = "Pegar"
        '
        'ToolStripMenuItem21
        '
        Me.ToolStripMenuItem21.Name = "ToolStripMenuItem21"
        Me.ToolStripMenuItem21.Size = New System.Drawing.Size(236, 6)
        '
        'CortarLineaToolMenu
        '
        Me.CortarLineaToolMenu.Image = CType(resources.GetObject("CortarLineaToolMenu.Image"), System.Drawing.Image)
        Me.CortarLineaToolMenu.Name = "CortarLineaToolMenu"
        Me.CortarLineaToolMenu.Size = New System.Drawing.Size(239, 22)
        Me.CortarLineaToolMenu.Text = "Envolver linea"
        '
        'SoloLecturaToolMenu
        '
        Me.SoloLecturaToolMenu.Image = CType(resources.GetObject("SoloLecturaToolMenu.Image"), System.Drawing.Image)
        Me.SoloLecturaToolMenu.Name = "SoloLecturaToolMenu"
        Me.SoloLecturaToolMenu.Size = New System.Drawing.Size(239, 22)
        Me.SoloLecturaToolMenu.Text = "Modo de solo lectura"
        '
        'SeleccionarTodoToolMenu
        '
        Me.SeleccionarTodoToolMenu.Image = CType(resources.GetObject("SeleccionarTodoToolMenu.Image"), System.Drawing.Image)
        Me.SeleccionarTodoToolMenu.Name = "SeleccionarTodoToolMenu"
        Me.SeleccionarTodoToolMenu.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.SeleccionarTodoToolMenu.Size = New System.Drawing.Size(239, 22)
        Me.SeleccionarTodoToolMenu.Text = "Seleccionar todo"
        '
        'ToolStripMenuItem8
        '
        Me.ToolStripMenuItem8.Name = "ToolStripMenuItem8"
        Me.ToolStripMenuItem8.Size = New System.Drawing.Size(236, 6)
        '
        'BuscarToolMenu
        '
        Me.BuscarToolMenu.Image = CType(resources.GetObject("BuscarToolMenu.Image"), System.Drawing.Image)
        Me.BuscarToolMenu.Name = "BuscarToolMenu"
        Me.BuscarToolMenu.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F), System.Windows.Forms.Keys)
        Me.BuscarToolMenu.Size = New System.Drawing.Size(239, 22)
        Me.BuscarToolMenu.Text = "Buscar"
        '
        'BuscarYReemplazarToolMenu
        '
        Me.BuscarYReemplazarToolMenu.Image = CType(resources.GetObject("BuscarYReemplazarToolMenu.Image"), System.Drawing.Image)
        Me.BuscarYReemplazarToolMenu.Name = "BuscarYReemplazarToolMenu"
        Me.BuscarYReemplazarToolMenu.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.H), System.Windows.Forms.Keys)
        Me.BuscarYReemplazarToolMenu.Size = New System.Drawing.Size(239, 22)
        Me.BuscarYReemplazarToolMenu.Text = "Buscar y reemplazar"
        '
        'IrALineaToolMenu
        '
        Me.IrALineaToolMenu.Image = CType(resources.GetObject("IrALineaToolMenu.Image"), System.Drawing.Image)
        Me.IrALineaToolMenu.Name = "IrALineaToolMenu"
        Me.IrALineaToolMenu.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.G), System.Windows.Forms.Keys)
        Me.IrALineaToolMenu.Size = New System.Drawing.Size(239, 22)
        Me.IrALineaToolMenu.Text = "Ir a la linea..."
        '
        'ToolStripMenuItem16
        '
        Me.ToolStripMenuItem16.Name = "ToolStripMenuItem16"
        Me.ToolStripMenuItem16.Size = New System.Drawing.Size(236, 6)
        '
        'PreferenciasToolMenu
        '
        Me.PreferenciasToolMenu.Image = CType(resources.GetObject("PreferenciasToolMenu.Image"), System.Drawing.Image)
        Me.PreferenciasToolMenu.Name = "PreferenciasToolMenu"
        Me.PreferenciasToolMenu.ShortcutKeys = System.Windows.Forms.Keys.F8
        Me.PreferenciasToolMenu.Size = New System.Drawing.Size(239, 22)
        Me.PreferenciasToolMenu.Text = "Preferencias"
        '
        'VerToolStripMenuItem
        '
        Me.VerToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ColorMenuItem, Me.ToolStripMenuItem6, Me.CambiarEstiloDeFuenteToolStripMenuItem, Me.AumentarFuenteMenuItem, Me.DisminuirFuenteMenuItem})
        Me.VerToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.VerToolStripMenuItem.Name = "VerToolStripMenuItem"
        Me.VerToolStripMenuItem.Size = New System.Drawing.Size(39, 20)
        Me.VerToolStripMenuItem.Text = "&Ver"
        '
        'ColorMenuItem
        '
        Me.ColorMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ColorDeLaFuenteToolStripMenuItem, Me.ToolStripMenuItem7, Me.DraculaTheme, Me.SolarOscuroTheme, Me.SolarClaroTheme, Me.ClassicTheme, Me.MoonTheme, Me.WarmCoffeeTheme, Me.KingdomTheme, Me.AuroraTheme, Me.CalmOceanTheme, Me.GrapeTheme, Me.BlackBoardTheme})
        Me.ColorMenuItem.Image = CType(resources.GetObject("ColorMenuItem.Image"), System.Drawing.Image)
        Me.ColorMenuItem.Name = "ColorMenuItem"
        Me.ColorMenuItem.Size = New System.Drawing.Size(288, 22)
        Me.ColorMenuItem.Text = "Esquemas de color"
        '
        'ColorDeLaFuenteToolStripMenuItem
        '
        Me.ColorDeLaFuenteToolStripMenuItem.Image = CType(resources.GetObject("ColorDeLaFuenteToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ColorDeLaFuenteToolStripMenuItem.Name = "ColorDeLaFuenteToolStripMenuItem"
        Me.ColorDeLaFuenteToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.ColorDeLaFuenteToolStripMenuItem.Text = "Color de la fuente"
        '
        'ToolStripMenuItem7
        '
        Me.ToolStripMenuItem7.Name = "ToolStripMenuItem7"
        Me.ToolStripMenuItem7.Size = New System.Drawing.Size(174, 6)
        '
        'DraculaTheme
        '
        Me.DraculaTheme.Image = CType(resources.GetObject("DraculaTheme.Image"), System.Drawing.Image)
        Me.DraculaTheme.Name = "DraculaTheme"
        Me.DraculaTheme.Size = New System.Drawing.Size(177, 22)
        Me.DraculaTheme.Text = "Dracula"
        '
        'SolarOscuroTheme
        '
        Me.SolarOscuroTheme.Image = CType(resources.GetObject("SolarOscuroTheme.Image"), System.Drawing.Image)
        Me.SolarOscuroTheme.Name = "SolarOscuroTheme"
        Me.SolarOscuroTheme.Size = New System.Drawing.Size(177, 22)
        Me.SolarOscuroTheme.Text = "Solar oscuro"
        '
        'SolarClaroTheme
        '
        Me.SolarClaroTheme.Image = CType(resources.GetObject("SolarClaroTheme.Image"), System.Drawing.Image)
        Me.SolarClaroTheme.Name = "SolarClaroTheme"
        Me.SolarClaroTheme.Size = New System.Drawing.Size(177, 22)
        Me.SolarClaroTheme.Text = "Solar claro"
        '
        'ClassicTheme
        '
        Me.ClassicTheme.Image = CType(resources.GetObject("ClassicTheme.Image"), System.Drawing.Image)
        Me.ClassicTheme.Name = "ClassicTheme"
        Me.ClassicTheme.Size = New System.Drawing.Size(177, 22)
        Me.ClassicTheme.Text = "Classic"
        '
        'MoonTheme
        '
        Me.MoonTheme.Image = CType(resources.GetObject("MoonTheme.Image"), System.Drawing.Image)
        Me.MoonTheme.Name = "MoonTheme"
        Me.MoonTheme.Size = New System.Drawing.Size(177, 22)
        Me.MoonTheme.Text = "Moon"
        '
        'WarmCoffeeTheme
        '
        Me.WarmCoffeeTheme.Image = CType(resources.GetObject("WarmCoffeeTheme.Image"), System.Drawing.Image)
        Me.WarmCoffeeTheme.Name = "WarmCoffeeTheme"
        Me.WarmCoffeeTheme.Size = New System.Drawing.Size(177, 22)
        Me.WarmCoffeeTheme.Text = "Warm Coffee"
        '
        'KingdomTheme
        '
        Me.KingdomTheme.Image = CType(resources.GetObject("KingdomTheme.Image"), System.Drawing.Image)
        Me.KingdomTheme.Name = "KingdomTheme"
        Me.KingdomTheme.Size = New System.Drawing.Size(177, 22)
        Me.KingdomTheme.Text = "Kingdom"
        '
        'AuroraTheme
        '
        Me.AuroraTheme.Image = CType(resources.GetObject("AuroraTheme.Image"), System.Drawing.Image)
        Me.AuroraTheme.Name = "AuroraTheme"
        Me.AuroraTheme.Size = New System.Drawing.Size(177, 22)
        Me.AuroraTheme.Text = "Aurora"
        '
        'CalmOceanTheme
        '
        Me.CalmOceanTheme.Image = CType(resources.GetObject("CalmOceanTheme.Image"), System.Drawing.Image)
        Me.CalmOceanTheme.Name = "CalmOceanTheme"
        Me.CalmOceanTheme.Size = New System.Drawing.Size(177, 22)
        Me.CalmOceanTheme.Text = "Calm Ocean"
        '
        'GrapeTheme
        '
        Me.GrapeTheme.Image = CType(resources.GetObject("GrapeTheme.Image"), System.Drawing.Image)
        Me.GrapeTheme.Name = "GrapeTheme"
        Me.GrapeTheme.Size = New System.Drawing.Size(177, 22)
        Me.GrapeTheme.Text = "Grape"
        '
        'BlackBoardTheme
        '
        Me.BlackBoardTheme.Image = CType(resources.GetObject("BlackBoardTheme.Image"), System.Drawing.Image)
        Me.BlackBoardTheme.Name = "BlackBoardTheme"
        Me.BlackBoardTheme.Size = New System.Drawing.Size(177, 22)
        Me.BlackBoardTheme.Text = "Black Board"
        '
        'ToolStripMenuItem6
        '
        Me.ToolStripMenuItem6.Name = "ToolStripMenuItem6"
        Me.ToolStripMenuItem6.Size = New System.Drawing.Size(285, 6)
        '
        'CambiarEstiloDeFuenteToolStripMenuItem
        '
        Me.CambiarEstiloDeFuenteToolStripMenuItem.Image = CType(resources.GetObject("CambiarEstiloDeFuenteToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CambiarEstiloDeFuenteToolStripMenuItem.Name = "CambiarEstiloDeFuenteToolStripMenuItem"
        Me.CambiarEstiloDeFuenteToolStripMenuItem.Size = New System.Drawing.Size(288, 22)
        Me.CambiarEstiloDeFuenteToolStripMenuItem.Text = "Cambiar estilo de fuente"
        '
        'AumentarFuenteMenuItem
        '
        Me.AumentarFuenteMenuItem.Image = CType(resources.GetObject("AumentarFuenteMenuItem.Image"), System.Drawing.Image)
        Me.AumentarFuenteMenuItem.Name = "AumentarFuenteMenuItem"
        Me.AumentarFuenteMenuItem.ShortcutKeyDisplayString = "Ctrl+Mayús.+Plus"
        Me.AumentarFuenteMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
            Or System.Windows.Forms.Keys.Oemplus), System.Windows.Forms.Keys)
        Me.AumentarFuenteMenuItem.Size = New System.Drawing.Size(288, 22)
        Me.AumentarFuenteMenuItem.Text = "Aumentar fuente"
        '
        'DisminuirFuenteMenuItem
        '
        Me.DisminuirFuenteMenuItem.Image = CType(resources.GetObject("DisminuirFuenteMenuItem.Image"), System.Drawing.Image)
        Me.DisminuirFuenteMenuItem.Name = "DisminuirFuenteMenuItem"
        Me.DisminuirFuenteMenuItem.ShortcutKeyDisplayString = "Ctrl+Mayús.+Minus"
        Me.DisminuirFuenteMenuItem.ShortcutKeys = CType(((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Shift) _
            Or System.Windows.Forms.Keys.OemMinus), System.Windows.Forms.Keys)
        Me.DisminuirFuenteMenuItem.Size = New System.Drawing.Size(288, 22)
        Me.DisminuirFuenteMenuItem.Text = "Disminuir fuente"
        '
        'AcercaDeToolStripMenuItem
        '
        Me.AcercaDeToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BashingAbout, Me.ToolStripMenuItem9, Me.VinariSoftwareAbout, Me.VinariOSAbout, Me.ToolStripMenuItem3, Me.CambiosEnEstaVersiónDeBashingToolStripMenuItem})
        Me.AcercaDeToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.AcercaDeToolStripMenuItem.Name = "AcercaDeToolStripMenuItem"
        Me.AcercaDeToolStripMenuItem.Size = New System.Drawing.Size(78, 20)
        Me.AcercaDeToolStripMenuItem.Text = "Acerca &De"
        '
        'BashingAbout
        '
        Me.BashingAbout.Image = CType(resources.GetObject("BashingAbout.Image"), System.Drawing.Image)
        Me.BashingAbout.Name = "BashingAbout"
        Me.BashingAbout.Size = New System.Drawing.Size(282, 22)
        Me.BashingAbout.Text = "Acerca de Bashing - Vinari Software"
        '
        'ToolStripMenuItem9
        '
        Me.ToolStripMenuItem9.Name = "ToolStripMenuItem9"
        Me.ToolStripMenuItem9.Size = New System.Drawing.Size(279, 6)
        '
        'VinariSoftwareAbout
        '
        Me.VinariSoftwareAbout.Image = CType(resources.GetObject("VinariSoftwareAbout.Image"), System.Drawing.Image)
        Me.VinariSoftwareAbout.Name = "VinariSoftwareAbout"
        Me.VinariSoftwareAbout.Size = New System.Drawing.Size(282, 22)
        Me.VinariSoftwareAbout.Text = "Acerca de Vinari Software"
        '
        'VinariOSAbout
        '
        Me.VinariOSAbout.Image = CType(resources.GetObject("VinariOSAbout.Image"), System.Drawing.Image)
        Me.VinariOSAbout.Name = "VinariOSAbout"
        Me.VinariOSAbout.Size = New System.Drawing.Size(282, 22)
        Me.VinariOSAbout.Text = "Acerca de Vinari OS"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(279, 6)
        '
        'CambiosEnEstaVersiónDeBashingToolStripMenuItem
        '
        Me.CambiosEnEstaVersiónDeBashingToolStripMenuItem.Image = CType(resources.GetObject("CambiosEnEstaVersiónDeBashingToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CambiosEnEstaVersiónDeBashingToolStripMenuItem.Name = "CambiosEnEstaVersiónDeBashingToolStripMenuItem"
        Me.CambiosEnEstaVersiónDeBashingToolStripMenuItem.Size = New System.Drawing.Size(282, 22)
        Me.CambiosEnEstaVersiónDeBashingToolStripMenuItem.Text = "Cambios en esta versión de Bashing"
        '
        'MenuStrip
        '
        Me.MenuStrip.BackColor = System.Drawing.Color.FromArgb(CType(CType(40, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.MenuStrip.Font = New System.Drawing.Font("Lucida Sans Unicode", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArchivoToolStripMenuItem, Me.EditarToolStripMenuItem, Me.LenguajeToolStripMenuItem, Me.VerToolStripMenuItem, Me.AcercaDeToolStripMenuItem})
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Size = New System.Drawing.Size(974, 24)
        Me.MenuStrip.TabIndex = 2
        Me.MenuStrip.Text = "MenuStrip"
        '
        'LenguajeToolStripMenuItem
        '
        Me.LenguajeToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BashLanguage, Me.CommandLanguage, Me.ToolStripMenuItem18, Me.RubyLanguage, Me.PythonLanguage, Me.ToolStripMenuItem19, Me.RustLanguage, Me.ValaLanguage, Me.LuaLanguage, Me.JavaLanguage, Me.ToolStripMenuItem20, Me.CppLanguage, Me.CLanguage, Me.ObjectiveCLanguage, Me.CSharpLanguage, Me.ToolStripMenuItem17, Me.PHPLanguage, Me.SQLLanguage, Me.JSLanguage, Me.CSSLanguage, Me.HTMLLanguage})
        Me.LenguajeToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.LenguajeToolStripMenuItem.Name = "LenguajeToolStripMenuItem"
        Me.LenguajeToolStripMenuItem.Size = New System.Drawing.Size(71, 20)
        Me.LenguajeToolStripMenuItem.Text = "&Lenguaje"
        '
        'BashLanguage
        '
        Me.BashLanguage.Image = CType(resources.GetObject("BashLanguage.Image"), System.Drawing.Image)
        Me.BashLanguage.Name = "BashLanguage"
        Me.BashLanguage.Size = New System.Drawing.Size(180, 22)
        Me.BashLanguage.Text = "Bash"
        '
        'CommandLanguage
        '
        Me.CommandLanguage.Image = CType(resources.GetObject("CommandLanguage.Image"), System.Drawing.Image)
        Me.CommandLanguage.Name = "CommandLanguage"
        Me.CommandLanguage.Size = New System.Drawing.Size(180, 22)
        Me.CommandLanguage.Text = "Command"
        '
        'ToolStripMenuItem18
        '
        Me.ToolStripMenuItem18.Name = "ToolStripMenuItem18"
        Me.ToolStripMenuItem18.Size = New System.Drawing.Size(177, 6)
        '
        'RubyLanguage
        '
        Me.RubyLanguage.Image = CType(resources.GetObject("RubyLanguage.Image"), System.Drawing.Image)
        Me.RubyLanguage.Name = "RubyLanguage"
        Me.RubyLanguage.Size = New System.Drawing.Size(180, 22)
        Me.RubyLanguage.Text = "Ruby"
        '
        'PythonLanguage
        '
        Me.PythonLanguage.Image = CType(resources.GetObject("PythonLanguage.Image"), System.Drawing.Image)
        Me.PythonLanguage.Name = "PythonLanguage"
        Me.PythonLanguage.Size = New System.Drawing.Size(180, 22)
        Me.PythonLanguage.Text = "Python"
        '
        'ToolStripMenuItem19
        '
        Me.ToolStripMenuItem19.Name = "ToolStripMenuItem19"
        Me.ToolStripMenuItem19.Size = New System.Drawing.Size(177, 6)
        '
        'RustLanguage
        '
        Me.RustLanguage.Image = CType(resources.GetObject("RustLanguage.Image"), System.Drawing.Image)
        Me.RustLanguage.Name = "RustLanguage"
        Me.RustLanguage.Size = New System.Drawing.Size(180, 22)
        Me.RustLanguage.Text = "Rust"
        '
        'ValaLanguage
        '
        Me.ValaLanguage.Image = CType(resources.GetObject("ValaLanguage.Image"), System.Drawing.Image)
        Me.ValaLanguage.Name = "ValaLanguage"
        Me.ValaLanguage.Size = New System.Drawing.Size(180, 22)
        Me.ValaLanguage.Text = "Vala"
        '
        'LuaLanguage
        '
        Me.LuaLanguage.Image = CType(resources.GetObject("LuaLanguage.Image"), System.Drawing.Image)
        Me.LuaLanguage.Name = "LuaLanguage"
        Me.LuaLanguage.Size = New System.Drawing.Size(180, 22)
        Me.LuaLanguage.Text = "Lua"
        '
        'JavaLanguage
        '
        Me.JavaLanguage.Image = CType(resources.GetObject("JavaLanguage.Image"), System.Drawing.Image)
        Me.JavaLanguage.Name = "JavaLanguage"
        Me.JavaLanguage.Size = New System.Drawing.Size(180, 22)
        Me.JavaLanguage.Text = "Java"
        '
        'ToolStripMenuItem20
        '
        Me.ToolStripMenuItem20.Name = "ToolStripMenuItem20"
        Me.ToolStripMenuItem20.Size = New System.Drawing.Size(177, 6)
        '
        'CppLanguage
        '
        Me.CppLanguage.Image = CType(resources.GetObject("CppLanguage.Image"), System.Drawing.Image)
        Me.CppLanguage.Name = "CppLanguage"
        Me.CppLanguage.Size = New System.Drawing.Size(180, 22)
        Me.CppLanguage.Text = "C++"
        '
        'CLanguage
        '
        Me.CLanguage.Image = CType(resources.GetObject("CLanguage.Image"), System.Drawing.Image)
        Me.CLanguage.Name = "CLanguage"
        Me.CLanguage.Size = New System.Drawing.Size(180, 22)
        Me.CLanguage.Text = "C"
        '
        'ObjectiveCLanguage
        '
        Me.ObjectiveCLanguage.Image = CType(resources.GetObject("ObjectiveCLanguage.Image"), System.Drawing.Image)
        Me.ObjectiveCLanguage.Name = "ObjectiveCLanguage"
        Me.ObjectiveCLanguage.Size = New System.Drawing.Size(180, 22)
        Me.ObjectiveCLanguage.Text = "Objective-C"
        '
        'CSharpLanguage
        '
        Me.CSharpLanguage.Image = CType(resources.GetObject("CSharpLanguage.Image"), System.Drawing.Image)
        Me.CSharpLanguage.Name = "CSharpLanguage"
        Me.CSharpLanguage.Size = New System.Drawing.Size(180, 22)
        Me.CSharpLanguage.Text = "C#"
        '
        'ToolStripMenuItem17
        '
        Me.ToolStripMenuItem17.Name = "ToolStripMenuItem17"
        Me.ToolStripMenuItem17.Size = New System.Drawing.Size(177, 6)
        '
        'PHPLanguage
        '
        Me.PHPLanguage.Image = CType(resources.GetObject("PHPLanguage.Image"), System.Drawing.Image)
        Me.PHPLanguage.Name = "PHPLanguage"
        Me.PHPLanguage.Size = New System.Drawing.Size(180, 22)
        Me.PHPLanguage.Text = "PHP"
        '
        'SQLLanguage
        '
        Me.SQLLanguage.Image = CType(resources.GetObject("SQLLanguage.Image"), System.Drawing.Image)
        Me.SQLLanguage.Name = "SQLLanguage"
        Me.SQLLanguage.Size = New System.Drawing.Size(180, 22)
        Me.SQLLanguage.Text = "SQL"
        '
        'JSLanguage
        '
        Me.JSLanguage.Image = CType(resources.GetObject("JSLanguage.Image"), System.Drawing.Image)
        Me.JSLanguage.Name = "JSLanguage"
        Me.JSLanguage.Size = New System.Drawing.Size(180, 22)
        Me.JSLanguage.Text = "JavaScript"
        '
        'CSSLanguage
        '
        Me.CSSLanguage.Image = CType(resources.GetObject("CSSLanguage.Image"), System.Drawing.Image)
        Me.CSSLanguage.Name = "CSSLanguage"
        Me.CSSLanguage.Size = New System.Drawing.Size(180, 22)
        Me.CSSLanguage.Text = "CSS"
        '
        'HTMLLanguage
        '
        Me.HTMLLanguage.Image = CType(resources.GetObject("HTMLLanguage.Image"), System.Drawing.Image)
        Me.HTMLLanguage.Name = "HTMLLanguage"
        Me.HTMLLanguage.Size = New System.Drawing.Size(180, 22)
        Me.HTMLLanguage.Text = "HTML"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Filter = resources.GetString("OpenFileDialog1.Filter")
        Me.OpenFileDialog1.Title = "Abrir Script"
        '
        'SaveFileDialog1
        '
        Me.SaveFileDialog1.Filter = resources.GetString("SaveFileDialog1.Filter")
        Me.SaveFileDialog1.Title = "Guardar Script"
        '
        'Script
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(974, 611)
        Me.Controls.Add(Me.FastColoredTextBox1)
        Me.Controls.Add(Me.MenuStrip)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(800, 600)
        Me.Name = "Script"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Bashing - Vinari Software"
        CType(Me.FastColoredTextBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.MenuStrip.ResumeLayout(False)
        Me.MenuStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents FastColoredTextBox1 As FastColoredTextBoxNS.FastColoredTextBox
    Friend WithEvents ColorDialog1 As ColorDialog
    Friend WithEvents ArchivoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AbrirScriptToolMenu As ToolStripMenuItem
    Friend WithEvents GuardarScriptComoToolMenu As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripSeparator
    Friend WithEvents NuevoDeocumentoToolMenu As ToolStripMenuItem
    Friend WithEvents EditarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DeshacerToolMenu As ToolStripMenuItem
    Friend WithEvents RehacerToolMenu As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem5 As ToolStripSeparator
    Friend WithEvents CopiarToolMenu As ToolStripMenuItem
    Friend WithEvents CortarToolMenu As ToolStripMenuItem
    Friend WithEvents PegarToolMenu As ToolStripMenuItem
    Friend WithEvents SeleccionarTodoToolMenu As ToolStripMenuItem
    Friend WithEvents VerToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ColorMenuItem As ToolStripMenuItem
    Friend WithEvents ColorDeLaFuenteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem7 As ToolStripSeparator
    Friend WithEvents SolarOscuroTheme As ToolStripMenuItem
    Friend WithEvents ClassicTheme As ToolStripMenuItem
    Friend WithEvents MoonTheme As ToolStripMenuItem
    Friend WithEvents WarmCoffeeTheme As ToolStripMenuItem
    Friend WithEvents KingdomTheme As ToolStripMenuItem
    Friend WithEvents AuroraTheme As ToolStripMenuItem
    Friend WithEvents AcercaDeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem9 As ToolStripSeparator
    Friend WithEvents BashingAbout As ToolStripMenuItem
    Friend WithEvents MenuStrip As MenuStrip
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
    Friend WithEvents ToolStripMenuItem8 As ToolStripSeparator
    Friend WithEvents BuscarYReemplazarToolMenu As ToolStripMenuItem
    Friend WithEvents IrALineaToolMenu As ToolStripMenuItem
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents DeshacerContext As ToolStripMenuItem
    Friend WithEvents RehacerContext As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripSeparator
    Friend WithEvents CopiarContext As ToolStripMenuItem
    Friend WithEvents CortarContext As ToolStripMenuItem
    Friend WithEvents PegarContext As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem10 As ToolStripSeparator
    Friend WithEvents SeleccionarTodoContext As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem11 As ToolStripSeparator
    Friend WithEvents BuscarYReemplazarContext As ToolStripMenuItem
    Friend WithEvents IrALaLineaContext As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem12 As ToolStripSeparator
    Friend WithEvents InformacionArchivoContext As ToolStripMenuItem
    Friend WithEvents BuscarToolMenu As ToolStripMenuItem
    Friend WithEvents BuscarContext As ToolStripMenuItem
    Friend WithEvents NuevoDocumentoContext As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As ToolStripSeparator
    Friend WithEvents GuardarScriptToolMenu As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem16 As ToolStripSeparator
    Friend WithEvents PreferenciasToolMenu As ToolStripMenuItem
    Friend WithEvents SoloLecturaToolMenu As ToolStripMenuItem
    Friend WithEvents LenguajeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BashLanguage As ToolStripMenuItem
    Friend WithEvents CommandLanguage As ToolStripMenuItem
    Friend WithEvents JavaLanguage As ToolStripMenuItem
    Friend WithEvents CppLanguage As ToolStripMenuItem
    Friend WithEvents ObjectiveCLanguage As ToolStripMenuItem
    Friend WithEvents JSLanguage As ToolStripMenuItem
    Friend WithEvents CLanguage As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem18 As ToolStripSeparator
    Friend WithEvents ToolStripMenuItem19 As ToolStripSeparator
    Friend WithEvents ToolStripMenuItem20 As ToolStripSeparator
    Friend WithEvents PythonLanguage As ToolStripMenuItem
    Friend WithEvents VinariSoftwareAbout As ToolStripMenuItem
    Friend WithEvents VinariOSAbout As ToolStripMenuItem
    Friend WithEvents NuevaInstanciaToolMenu As ToolStripMenuItem
    Friend WithEvents NuevaInstanciaContext As ToolStripMenuItem
    Friend WithEvents CalmOceanTheme As ToolStripMenuItem
    Friend WithEvents RubyLanguage As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem17 As ToolStripSeparator
    Friend WithEvents PHPLanguage As ToolStripMenuItem
    Friend WithEvents CSSLanguage As ToolStripMenuItem
    Friend WithEvents HTMLLanguage As ToolStripMenuItem
    Friend WithEvents CSharpLanguage As ToolStripMenuItem
    Friend WithEvents CortarLineaToolMenu As ToolStripMenuItem
    Friend WithEvents GrapeTheme As ToolStripMenuItem
    Friend WithEvents BlackBoardTheme As ToolStripMenuItem
    Friend WithEvents ValaLanguage As ToolStripMenuItem
    Friend WithEvents CambiarEstiloDeFuenteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SolarClaroTheme As ToolStripMenuItem
    Friend WithEvents LuaLanguage As ToolStripMenuItem
    Friend WithEvents SQLLanguage As ToolStripMenuItem
    Friend WithEvents DraculaTheme As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem21 As ToolStripSeparator
    Friend WithEvents ToolStripMenuItem6 As ToolStripSeparator
    Friend WithEvents AumentarFuenteMenuItem As ToolStripMenuItem
    Friend WithEvents DisminuirFuenteMenuItem As ToolStripMenuItem
    Friend WithEvents RustLanguage As ToolStripMenuItem
    Friend WithEvents CambiosEnEstaVersiónDeBashingToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem13 As ToolStripSeparator
    Friend WithEvents SalirToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As ToolStripSeparator
    Friend WithEvents PrintDocument1 As Printing.PrintDocument
End Class
