﻿Public NotInheritable Class AboutBox1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Licencia.Show()
    End Sub

    Private Sub AboutBox1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If My.Settings.Language = "en" Then
            RichTextBoxEN.Visible = True
            RichTextBoxES.Visible = False
            Button1.Text = "Accept"
            Button2.Text = "License agreement"
            Me.Text = "Bashing - Vinari Software | About"
        End If

        Label1.Text = "Version: " & Application.ProductVersion
    End Sub
End Class
