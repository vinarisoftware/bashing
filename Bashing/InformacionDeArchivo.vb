﻿Imports System.IO
Public Class InformacionDeArchivo

    Private FileExtension As String

    Private Sub InformacionDeArchivo_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If My.Settings.Language = "en" Then
            Me.Text = "Bashing - Vinari Software| File info"

            Label1.Text = "Current file location:"
            Label2.Text = "Type of file loaded:"
            Label3.Text = "Codification of the file loaded:"
            Label4.Text = "The language of the file has not been determined."
            Label5.Text = "The codification of the file has not been determinated."

            Button1.Text = "Copy file location to clipboard"
            Button2.Text = "Accept"

        End If

        If Script.OpenFileDialog1.FileName.Length = 3 Then
            TextBox1.Text = "..."
        ElseIf Script.OpenFileDialog1.FileName.Length > 3 Then
            TextBox1.Text = Script.OpenFileDialog1.FileName
        End If

        FileExtension = Path.GetExtension(Script.OpenFileDialog1.FileName).ToLower()

        Select Case FileExtension
            Case ".sh"
                Label4.Text = "*.sh | Bash (Linux, FreeBSD, UNIX, macOS, etc.)."
            Case ".command"
                Label4.Text = "*.command | Bash (macOS)."
            Case ".rb"
                Label4.Text = "*.rb | Ruby."
            Case ".py"
                Label4.Text = "*.py | Phyton."
            Case ".vala"
                Label4.Text = "*.vala | Vala."
            Case ".java"
                Label4.Text = "*.java | Java."
            Case ".cpp"
                Label4.Text = "*.cpp | C++."
            Case ".cxx"
                Label4.Text = "*.cxx | C++."
            Case ".h"
                Label4.Text = "*.h | C++."
            Case ".c"
                Label4.Text = "*.c | C."
            Case ".m"
                Label4.Text = "*.m | Objective-C."
            Case ".mm"
                Label4.Text = "*.mm | Objective-C."
            Case ".cs"
                Label4.Text = "*.cs | C#."
            Case ".php"
                Label4.Text = "*.php | PHP."
            Case ".js"
                Label4.Text = "*.js | JavaScript."
            Case ".css"
                Label4.Text = "*.css | CSS."
            Case ".html"
                Label4.Text = "*.html | HTML."
            Case ".txt"
                Label4.Text = "*.txt"

        End Select

        Try
            If Script.OpenFileDialog1.FileName.Length = 3 Then
                'DEJAR VACIO
            ElseIf Script.OpenFileDialog1.FileName.Length > 3 Then
                Dim FileEncode As New StreamWriter(Script.OpenFileDialog1.FileName)
                Label5.Text = FileEncode.Encoding.ToString
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        TextBox1.Copy()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

End Class