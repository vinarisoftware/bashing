﻿Imports System.IO
Public Class Script

    Private FileToExec As String
    Private SavedFile As String = ""
    Private Sub Script_Closing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        If FastColoredTextBox1.Text.Length > 0 Then

            Dim Salida As DialogResult = MessageBox.Show("Antes de salir ¿Desea guardar los cambios en el archivo?", "Bashing - Vinari Software", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)
            If Salida = DialogResult.Yes Then
                SaveFileDialog1.ShowDialog()
            ElseIf Salida = DialogResult.No Then
                'DEJAR VACIO
            ElseIf Salida = DialogResult.Cancel Then
                e.Cancel = True
            End If
        End If

        My.Settings.ScriptWindowState = Me.WindowState
        My.Settings.FontType = FastColoredTextBox1.Font
        My.Settings.ScriptSize = Me.Size
        My.Settings.ScriptLocation = Me.Location
        My.Settings.Save()

    End Sub

    Private Sub NuevoDeocumentoEnBlancoDeTipoScriptToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NuevoDeocumentoToolMenu.Click
        Dim NuevoScript = MsgBox("¿Desea crear un documento nuevo? (Todo el trabajo no guardado se perderá)", MsgBoxStyle.YesNo)
        If NuevoScript = DialogResult.Yes Then
            Me.Text = "Bashing - Vinari Software v" & Application.ProductVersion
            FastColoredTextBox1.Clear()
            GuardarScriptToolMenu.Enabled = False
            OpenFileDialog1.FileName = ""
            restartFCTB()
        End If
    End Sub

    Private Sub DeshacerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeshacerToolMenu.Click
        FastColoredTextBox1.Undo()
    End Sub

    Private Sub RehacerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RehacerToolMenu.Click
        FastColoredTextBox1.Redo()
    End Sub

    Private Sub CopiarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopiarToolMenu.Click
        FastColoredTextBox1.Copy()
    End Sub

    Private Sub CortarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CortarToolMenu.Click
        FastColoredTextBox1.Cut()
    End Sub

    Private Sub PegarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PegarToolMenu.Click
        FastColoredTextBox1.Paste()
    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SeleccionarTodoToolMenu.Click
        FastColoredTextBox1.SelectAll()
    End Sub

    Private Sub BuscarYReemplazarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BuscarYReemplazarToolMenu.Click
        FastColoredTextBox1.ShowReplaceDialog()
    End Sub

    Private Sub BuscarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BuscarToolMenu.Click
        FastColoredTextBox1.ShowFindDialog()
    End Sub

    Private Sub IrALineaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles IrALineaToolMenu.Click
        FastColoredTextBox1.ShowGoToDialog()
    End Sub

    Private Sub AcercaDeBashingToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BashingAbout.Click
        AboutBox1.Show()
    End Sub
    Private Sub SolarOscuroTheme_Click(sender As Object, e As EventArgs) Handles SolarOscuroTheme.Click
        MenuStrip.BackColor = Color.FromArgb(0, 43, 54)
        FastColoredTextBox1.ForeColor = Color.FromArgb(131, 148, 150)
        FastColoredTextBox1.BackColor = Color.FromArgb(0, 43, 54)
        FastColoredTextBox1.IndentBackColor = Color.FromArgb(0, 56, 71)
        FastColoredTextBox1.LineNumberColor = Color.FromArgb(153, 175, 181)
        FastColoredTextBox1.CurrentLineColor = Color.DarkGray

        ArchivoToolStripMenuItem.ForeColor = Color.White
        EditarToolStripMenuItem.ForeColor = Color.White
        VerToolStripMenuItem.ForeColor = Color.White
        AcercaDeToolStripMenuItem.ForeColor = Color.White
        LenguajeToolStripMenuItem.ForeColor = Color.White

        My.Settings.BackgroundFCTBScript = FastColoredTextBox1.BackColor
        My.Settings.FontColorFCTBScript = FastColoredTextBox1.ForeColor
        My.Settings.IndentColorFCTBScript = FastColoredTextBox1.IndentBackColor
        My.Settings.LineNumberColorFCTBScript = FastColoredTextBox1.LineNumberColor
        My.Settings.CurrentLineColorFCTBScript = Me.FastColoredTextBox1.CurrentLineColor

        My.Settings.BackgroundMenuStripScript = Me.MenuStrip.BackColor
        My.Settings.FontColorMenuStripScript = Me.MenuStrip.ForeColor

        My.Settings.FontColorMenuStripScript = Me.ArchivoToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.EditarToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.VerToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.AcercaDeToolStripMenuItem.ForeColor

        My.Settings.Save()
    End Sub

    Private Sub ClassicToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ClassicTheme.Click
        MenuStrip.BackColor = Color.MintCream
        ArchivoToolStripMenuItem.ForeColor = Color.Black
        EditarToolStripMenuItem.ForeColor = Color.Black
        VerToolStripMenuItem.ForeColor = Color.Black
        AcercaDeToolStripMenuItem.ForeColor = Color.Black
        LenguajeToolStripMenuItem.ForeColor = Color.Black

        FastColoredTextBox1.ForeColor = Color.Black
        FastColoredTextBox1.BackColor = Color.MintCream
        FastColoredTextBox1.IndentBackColor = Color.DarkGray
        FastColoredTextBox1.LineNumberColor = Color.Black
        FastColoredTextBox1.CurrentLineColor = Color.DarkGray

        My.Settings.BackgroundFCTBScript = FastColoredTextBox1.BackColor
        My.Settings.FontColorFCTBScript = FastColoredTextBox1.ForeColor
        My.Settings.IndentColorFCTBScript = FastColoredTextBox1.IndentBackColor
        My.Settings.LineNumberColorFCTBScript = FastColoredTextBox1.LineNumberColor
        My.Settings.CurrentLineColorFCTBScript = Me.FastColoredTextBox1.CurrentLineColor

        My.Settings.BackgroundMenuStripScript = Me.MenuStrip.BackColor
        My.Settings.FontColorMenuStripScript = Me.MenuStrip.ForeColor

        My.Settings.FontColorMenuStripScript = Me.ArchivoToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.EditarToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.VerToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.AcercaDeToolStripMenuItem.ForeColor

        My.Settings.Save()
    End Sub

    Private Sub MoonToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MoonTheme.Click
        MenuStrip.BackColor = Color.FromArgb(39, 40, 34)
        FastColoredTextBox1.ForeColor = Color.White
        FastColoredTextBox1.BackColor = Color.FromArgb(39, 40, 34)
        FastColoredTextBox1.IndentBackColor = Color.FromArgb(108, 109, 105)
        FastColoredTextBox1.LineNumberColor = Color.FloralWhite
        FastColoredTextBox1.CurrentLineColor = Color.DarkGray

        ArchivoToolStripMenuItem.ForeColor = Color.White
        EditarToolStripMenuItem.ForeColor = Color.White
        VerToolStripMenuItem.ForeColor = Color.White
        AcercaDeToolStripMenuItem.ForeColor = Color.White
        LenguajeToolStripMenuItem.ForeColor = Color.White

        My.Settings.BackgroundFCTBScript = FastColoredTextBox1.BackColor
        My.Settings.FontColorFCTBScript = FastColoredTextBox1.ForeColor
        My.Settings.IndentColorFCTBScript = FastColoredTextBox1.IndentBackColor
        My.Settings.LineNumberColorFCTBScript = FastColoredTextBox1.LineNumberColor
        My.Settings.CurrentLineColorFCTBScript = Me.FastColoredTextBox1.CurrentLineColor

        My.Settings.BackgroundMenuStripScript = Me.MenuStrip.BackColor
        My.Settings.FontColorMenuStripScript = Me.MenuStrip.ForeColor

        My.Settings.FontColorMenuStripScript = Me.ArchivoToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.EditarToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.VerToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.AcercaDeToolStripMenuItem.ForeColor

        My.Settings.Save()
    End Sub

    Private Sub WarmCoffeeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles WarmCoffeeTheme.Click
        MenuStrip.BackColor = Color.FromArgb(34, 26, 15)
        FastColoredTextBox1.ForeColor = Color.FromArgb(211, 175, 134)
        FastColoredTextBox1.BackColor = Color.FromArgb(34, 26, 15)
        FastColoredTextBox1.IndentBackColor = Color.FromArgb(94, 64, 43)
        FastColoredTextBox1.LineNumberColor = Color.FromArgb(211, 142, 59)
        FastColoredTextBox1.CurrentLineColor = Color.DarkGray

        ArchivoToolStripMenuItem.ForeColor = Color.White
        EditarToolStripMenuItem.ForeColor = Color.White
        VerToolStripMenuItem.ForeColor = Color.White
        AcercaDeToolStripMenuItem.ForeColor = Color.White
        LenguajeToolStripMenuItem.ForeColor = Color.White

        My.Settings.BackgroundFCTBScript = FastColoredTextBox1.BackColor
        My.Settings.FontColorFCTBScript = FastColoredTextBox1.ForeColor
        My.Settings.IndentColorFCTBScript = FastColoredTextBox1.IndentBackColor
        My.Settings.LineNumberColorFCTBScript = FastColoredTextBox1.LineNumberColor
        My.Settings.CurrentLineColorFCTBScript = Me.FastColoredTextBox1.CurrentLineColor

        My.Settings.BackgroundMenuStripScript = Me.MenuStrip.BackColor
        My.Settings.FontColorMenuStripScript = Me.MenuStrip.ForeColor

        My.Settings.FontColorMenuStripScript = Me.ArchivoToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.EditarToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.VerToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.AcercaDeToolStripMenuItem.ForeColor

        My.Settings.Save()
    End Sub

    Private Sub KingdomToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KingdomTheme.Click
        MenuStrip.BackColor = Color.FromArgb(0, 12, 24)
        FastColoredTextBox1.ForeColor = Color.FromArgb(103, 181, 181)
        FastColoredTextBox1.BackColor = Color.FromArgb(0, 12, 24)
        FastColoredTextBox1.IndentBackColor = Color.FromArgb(0, 36, 81)
        FastColoredTextBox1.LineNumberColor = Color.FromArgb(150, 160, 175)
        FastColoredTextBox1.CurrentLineColor = Color.DarkGray

        ArchivoToolStripMenuItem.ForeColor = Color.White
        EditarToolStripMenuItem.ForeColor = Color.White
        VerToolStripMenuItem.ForeColor = Color.White
        AcercaDeToolStripMenuItem.ForeColor = Color.White
        LenguajeToolStripMenuItem.ForeColor = Color.White

        My.Settings.BackgroundFCTBScript = FastColoredTextBox1.BackColor
        My.Settings.FontColorFCTBScript = FastColoredTextBox1.ForeColor
        My.Settings.IndentColorFCTBScript = FastColoredTextBox1.IndentBackColor
        My.Settings.LineNumberColorFCTBScript = FastColoredTextBox1.LineNumberColor
        My.Settings.CurrentLineColorFCTBScript = Me.FastColoredTextBox1.CurrentLineColor

        My.Settings.BackgroundMenuStripScript = Me.MenuStrip.BackColor
        My.Settings.FontColorMenuStripScript = Me.MenuStrip.ForeColor

        My.Settings.FontColorMenuStripScript = Me.ArchivoToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.EditarToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.VerToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.AcercaDeToolStripMenuItem.ForeColor

        My.Settings.Save()
    End Sub

    Private Sub AuroraToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AuroraTheme.Click
        MenuStrip.BackColor = Color.FromArgb(47, 41, 87)
        FastColoredTextBox1.ForeColor = Color.White
        FastColoredTextBox1.BackColor = Color.FromArgb(47, 41, 87)
        FastColoredTextBox1.IndentBackColor = Color.FromArgb(34, 34, 70)
        FastColoredTextBox1.LineNumberColor = Color.FromArgb(137, 80, 95)
        FastColoredTextBox1.CurrentLineColor = Color.DarkGray

        ArchivoToolStripMenuItem.ForeColor = Color.White
        EditarToolStripMenuItem.ForeColor = Color.White
        VerToolStripMenuItem.ForeColor = Color.White
        AcercaDeToolStripMenuItem.ForeColor = Color.White
        LenguajeToolStripMenuItem.ForeColor = Color.White

        My.Settings.BackgroundFCTBScript = FastColoredTextBox1.BackColor
        My.Settings.FontColorFCTBScript = FastColoredTextBox1.ForeColor
        My.Settings.IndentColorFCTBScript = FastColoredTextBox1.IndentBackColor
        My.Settings.LineNumberColorFCTBScript = FastColoredTextBox1.LineNumberColor
        My.Settings.CurrentLineColorFCTBScript = Me.FastColoredTextBox1.CurrentLineColor

        My.Settings.BackgroundMenuStripScript = Me.MenuStrip.BackColor
        My.Settings.FontColorMenuStripScript = Me.MenuStrip.ForeColor

        My.Settings.FontColorMenuStripScript = Me.ArchivoToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.EditarToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.VerToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.AcercaDeToolStripMenuItem.ForeColor

        My.Settings.Save()
    End Sub
    Private Sub CalmOceanToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CalmOceanTheme.Click
        MenuStrip.BackColor = Color.FromArgb(29, 38, 47)
        FastColoredTextBox1.ForeColor = Color.FromArgb(52, 254, 186)
        FastColoredTextBox1.BackColor = Color.FromArgb(29, 38, 47)
        FastColoredTextBox1.IndentBackColor = Color.FromArgb(137, 180, 231)
        FastColoredTextBox1.LineNumberColor = Color.FromArgb(81, 83, 84)
        FastColoredTextBox1.CurrentLineColor = Color.DarkGray

        ArchivoToolStripMenuItem.ForeColor = Color.White
        EditarToolStripMenuItem.ForeColor = Color.White
        VerToolStripMenuItem.ForeColor = Color.White
        AcercaDeToolStripMenuItem.ForeColor = Color.White
        LenguajeToolStripMenuItem.ForeColor = Color.White

        My.Settings.BackgroundFCTBScript = FastColoredTextBox1.BackColor
        My.Settings.FontColorFCTBScript = FastColoredTextBox1.ForeColor
        My.Settings.IndentColorFCTBScript = FastColoredTextBox1.IndentBackColor
        My.Settings.LineNumberColorFCTBScript = FastColoredTextBox1.LineNumberColor
        My.Settings.CurrentLineColorFCTBScript = Me.FastColoredTextBox1.CurrentLineColor

        My.Settings.BackgroundMenuStripScript = Me.MenuStrip.BackColor
        My.Settings.FontColorMenuStripScript = Me.MenuStrip.ForeColor

        My.Settings.FontColorMenuStripScript = Me.ArchivoToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.EditarToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.VerToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.AcercaDeToolStripMenuItem.ForeColor

        My.Settings.Save()
    End Sub
    Private Sub DraculaTheme_Click(sender As Object, e As EventArgs) Handles DraculaTheme.Click
        MenuStrip.BackColor = Color.FromArgb(40, 42, 54)
        FastColoredTextBox1.ForeColor = Color.White
        FastColoredTextBox1.BackColor = Color.FromArgb(40, 42, 54)
        FastColoredTextBox1.IndentBackColor = Color.FromArgb(44, 40, 47)
        FastColoredTextBox1.LineNumberColor = Color.FromArgb(139, 233, 225)
        FastColoredTextBox1.CurrentLineColor = Color.FromArgb(70, 73, 92)

        ArchivoToolStripMenuItem.ForeColor = Color.White
        EditarToolStripMenuItem.ForeColor = Color.White
        VerToolStripMenuItem.ForeColor = Color.White
        AcercaDeToolStripMenuItem.ForeColor = Color.White
        LenguajeToolStripMenuItem.ForeColor = Color.White

        My.Settings.BackgroundFCTBScript = FastColoredTextBox1.BackColor
        My.Settings.FontColorFCTBScript = FastColoredTextBox1.ForeColor
        My.Settings.IndentColorFCTBScript = FastColoredTextBox1.IndentBackColor
        My.Settings.LineNumberColorFCTBScript = FastColoredTextBox1.LineNumberColor
        My.Settings.CurrentLineColorFCTBScript = Me.FastColoredTextBox1.CurrentLineColor

        My.Settings.BackgroundMenuStripScript = Me.MenuStrip.BackColor
        My.Settings.FontColorMenuStripScript = Me.MenuStrip.ForeColor

        My.Settings.FontColorMenuStripScript = Me.ArchivoToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.EditarToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.VerToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.AcercaDeToolStripMenuItem.ForeColor

        My.Settings.Save()
    End Sub
    Private Sub GrapeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GrapeTheme.Click
        MenuStrip.BackColor = Color.FromArgb(45, 4, 41)
        FastColoredTextBox1.ForeColor = Color.FromArgb(217, 219, 252)
        FastColoredTextBox1.BackColor = Color.FromArgb(45, 4, 41)
        FastColoredTextBox1.IndentBackColor = Color.FromArgb(80, 47, 76)
        FastColoredTextBox1.LineNumberColor = Color.FromArgb(124, 124, 144)
        FastColoredTextBox1.CurrentLineColor = Color.DarkGray

        ArchivoToolStripMenuItem.ForeColor = Color.White
        EditarToolStripMenuItem.ForeColor = Color.White
        VerToolStripMenuItem.ForeColor = Color.White
        AcercaDeToolStripMenuItem.ForeColor = Color.White
        LenguajeToolStripMenuItem.ForeColor = Color.White

        My.Settings.BackgroundFCTBScript = FastColoredTextBox1.BackColor
        My.Settings.FontColorFCTBScript = FastColoredTextBox1.ForeColor
        My.Settings.IndentColorFCTBScript = FastColoredTextBox1.IndentBackColor
        My.Settings.LineNumberColorFCTBScript = FastColoredTextBox1.LineNumberColor
        My.Settings.CurrentLineColorFCTBScript = Me.FastColoredTextBox1.CurrentLineColor

        My.Settings.BackgroundMenuStripScript = Me.MenuStrip.BackColor
        My.Settings.FontColorMenuStripScript = Me.MenuStrip.ForeColor

        My.Settings.FontColorMenuStripScript = Me.ArchivoToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.EditarToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.VerToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.AcercaDeToolStripMenuItem.ForeColor

        My.Settings.Save()
    End Sub

    Private Sub BlackBoardToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BlackBoardTheme.Click
        MenuStrip.BackColor = Color.FromArgb(0, 0, 0)
        FastColoredTextBox1.ForeColor = Color.FromArgb(167, 78, 197)
        FastColoredTextBox1.BackColor = Color.FromArgb(0, 0, 0)
        FastColoredTextBox1.IndentBackColor = Color.FromArgb(44, 44, 44)
        FastColoredTextBox1.LineNumberColor = Color.FromArgb(146, 146, 146)
        FastColoredTextBox1.CurrentLineColor = Color.DarkGray

        ArchivoToolStripMenuItem.ForeColor = Color.White
        EditarToolStripMenuItem.ForeColor = Color.White
        VerToolStripMenuItem.ForeColor = Color.White
        AcercaDeToolStripMenuItem.ForeColor = Color.White
        LenguajeToolStripMenuItem.ForeColor = Color.White

        My.Settings.BackgroundFCTBScript = FastColoredTextBox1.BackColor
        My.Settings.FontColorFCTBScript = FastColoredTextBox1.ForeColor
        My.Settings.IndentColorFCTBScript = FastColoredTextBox1.IndentBackColor
        My.Settings.LineNumberColorFCTBScript = FastColoredTextBox1.LineNumberColor
        My.Settings.CurrentLineColorFCTBScript = Me.FastColoredTextBox1.CurrentLineColor

        My.Settings.BackgroundMenuStripScript = Me.MenuStrip.BackColor
        My.Settings.FontColorMenuStripScript = Me.MenuStrip.ForeColor

        My.Settings.FontColorMenuStripScript = Me.ArchivoToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.EditarToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.VerToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.AcercaDeToolStripMenuItem.ForeColor

        My.Settings.Save()
    End Sub

    Private Sub SolarizadoClaroTheme_Click(sender As Object, e As EventArgs) Handles SolarClaroTheme.Click
        MenuStrip.BackColor = Color.FromArgb(253, 246, 227)
        FastColoredTextBox1.ForeColor = Color.FromArgb(101, 123, 131)
        FastColoredTextBox1.BackColor = Color.FromArgb(253, 246, 227)
        FastColoredTextBox1.IndentBackColor = Color.FromArgb(238, 232, 213)
        FastColoredTextBox1.LineNumberColor = Color.FromArgb(147, 161, 178)
        FastColoredTextBox1.CurrentLineColor = Color.FromArgb(238, 232, 213)

        ArchivoToolStripMenuItem.ForeColor = Color.Gray
        EditarToolStripMenuItem.ForeColor = Color.Gray
        VerToolStripMenuItem.ForeColor = Color.Gray
        AcercaDeToolStripMenuItem.ForeColor = Color.Gray
        LenguajeToolStripMenuItem.ForeColor = Color.Gray

        My.Settings.BackgroundFCTBScript = FastColoredTextBox1.BackColor
        My.Settings.FontColorFCTBScript = FastColoredTextBox1.ForeColor
        My.Settings.IndentColorFCTBScript = FastColoredTextBox1.IndentBackColor
        My.Settings.LineNumberColorFCTBScript = FastColoredTextBox1.LineNumberColor
        My.Settings.CurrentLineColorFCTBScript = Me.FastColoredTextBox1.CurrentLineColor

        My.Settings.BackgroundMenuStripScript = Me.MenuStrip.BackColor
        My.Settings.FontColorMenuStripScript = Me.MenuStrip.ForeColor

        My.Settings.FontColorMenuStripScript = Me.ArchivoToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.EditarToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.VerToolStripMenuItem.ForeColor
        My.Settings.FontColorMenuStripScript = Me.AcercaDeToolStripMenuItem.ForeColor

        My.Settings.Save()
    End Sub

    Private Sub AbrirScriptToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AbrirScriptToolMenu.Click
        If OpenFileDialog1.ShowDialog() = DialogResult.OK Then
            Me.Text = System.IO.Path.GetFileName(OpenFileDialog1.FileName) + "  | Bashing - Vinari Software v" & Application.ProductVersion
            GuardarScriptToolMenu.Enabled = True
        End If
    End Sub

    Private Sub GuardarScriptToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GuardarScriptComoToolMenu.Click
        If SaveFileDialog1.ShowDialog() = DialogResult.OK Then
            GuardarScriptToolMenu.Enabled = True
        End If
    End Sub


    Private Sub SaveFileDialog1_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles SaveFileDialog1.FileOk
        Try
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, FastColoredTextBox1.Text, False)
        Catch ex As Exception
        End Try

        Select Case Path.GetExtension(SaveFileDialog1.FileName).ToLower()
            Case ".sh"
                BashLanguage.PerformClick()
                MsgBox("Por favor, considere la distribución GNU/Linux para la cual escribió el script y, además, considere que deberá usar dos2unix para ejecutar el script en una terminal", MessageBoxIcon.Warning)
            Case ".command"
                CommandLanguage.PerformClick()
                MsgBox("Por favor, considere que para ejecutar o el script que escribió necesitara de un Mac, corriendo cualquier versión de macOS (10.0 en adelante)", MessageBoxIcon.Warning)
            Case ".rb"
                RubyLanguage.PerformClick()
            Case ".py"
                PythonLanguage.PerformClick()
            Case ".vala"
                ValaLanguage.PerformClick()
            Case ".java"
                JavaLanguage.PerformClick()
            Case ".cpp"
                CppLanguage.PerformClick()
            Case ".cxx"
                CppLanguage.PerformClick()
            Case ".h"
                CppLanguage.PerformClick()
            Case ".c"
                CLanguage.PerformClick()
            Case ".sql"
                SQLLanguage.PerformClick()
            Case ".lua"
                LuaLanguage.PerformClick()
            Case ".m"
                ObjectiveCLanguage.PerformClick()
            Case ".mm"
                ObjectiveCLanguage.PerformClick()
            Case ".cs"
                CSharpLanguage.PerformClick()
            Case ".php"
                PHPLanguage.PerformClick()
            Case ".js"
                JSLanguage.PerformClick()
            Case ".css"
                CSSLanguage.PerformClick()
            Case ".html"
                HTMLLanguage.PerformClick()
            Case ".htm"
                HTMLLanguage.PerformClick()
            Case ".hta"
                HTMLLanguage.PerformClick()
        End Select

        SavedFile = SaveFileDialog1.FileName
        SaveFileDialog1.FileName = System.IO.Path.GetFileName(SaveFileDialog1.FileName)

    End Sub

    Private Sub OpenFileDialog1_FileOk(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog1.FileOk
        Try
            FastColoredTextBox1.Text = My.Computer.FileSystem.ReadAllText(OpenFileDialog1.FileName)
            FastColoredTextBox1.Update()
            SavedFile = OpenFileDialog1.FileName
        Catch ex As Exception
        End Try

        Select Case Path.GetExtension(OpenFileDialog1.FileName).ToLower()
            Case ".sh"
                BashLanguage.PerformClick()
            Case ".command"
                CommandLanguage.PerformClick()
            Case ".rb"
                RubyLanguage.PerformClick()
            Case ".py"
                PythonLanguage.PerformClick()
            Case ".vala"
                ValaLanguage.PerformClick()
            Case ".rs"
                RustLanguage.PerformClick()
            Case ".java"
                JavaLanguage.PerformClick()
            Case ".cpp"
                CppLanguage.PerformClick()
            Case ".cxx"
                CppLanguage.PerformClick()
            Case ".h"
                CppLanguage.PerformClick()
            Case ".c"
                CLanguage.PerformClick()
            Case ".sql"
                SQLLanguage.PerformClick()
            Case ".lua"
                LuaLanguage.PerformClick()
            Case ".m"
                ObjectiveCLanguage.PerformClick()
            Case ".mm"
                ObjectiveCLanguage.PerformClick()
            Case ".cs"
                CSharpLanguage.PerformClick()
            Case ".php"
                PHPLanguage.PerformClick()
            Case ".js"
                JSLanguage.PerformClick()
            Case ".css"
                CSSLanguage.PerformClick()
            Case ".html"
                HTMLLanguage.PerformClick()
            Case ".htm"
                HTMLLanguage.PerformClick()
            Case ".hta"
                HTMLLanguage.PerformClick()
        End Select
    End Sub

    Private Sub DeshacerToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles DeshacerContext.Click
        DeshacerToolMenu.PerformClick()
    End Sub

    Private Sub RehacerToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles RehacerContext.Click
        RehacerToolMenu.PerformClick()
    End Sub

    Private Sub CopiarToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles CopiarContext.Click
        CopiarToolMenu.PerformClick()
    End Sub

    Private Sub CortarToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles CortarContext.Click
        FastColoredTextBox1.Cut()
    End Sub

    Private Sub PegarToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles PegarContext.Click
        PegarToolMenu.PerformClick()
    End Sub

    Private Sub SeleccionarTodoToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles SeleccionarTodoContext.Click
        SeleccionarTodoToolMenu.PerformClick()
    End Sub

    Private Sub BuscarYReemplazarToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles BuscarYReemplazarContext.Click
        BuscarYReemplazarToolMenu.PerformClick()
    End Sub

    Private Sub IrALaLineaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles IrALaLineaContext.Click
        IrALineaToolMenu.PerformClick()
    End Sub

    Private Sub BuscarContext_Click_1(sender As Object, e As EventArgs) Handles BuscarContext.Click
        BuscarToolMenu.PerformClick()
    End Sub

    Private Sub VerUbicaciónDelArchivoToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles InformacionArchivoContext.Click
        InformacionDeArchivo.Show()
    End Sub

    Private Sub NuevoDocumentoDeTipoScriptToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NuevoDocumentoContext.Click
        NuevoDeocumentoToolMenu.PerformClick()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Size = My.Settings.ScriptSize
        FastColoredTextBox1.Font = My.Settings.FontType
        FastColoredTextBox1.TabLength = My.Settings.TABSize
        FastColoredTextBox1.AutoCompleteBrackets = My.Settings.AutoCompleteBrackets
        FastColoredTextBox1.AutoIndent = My.Settings.AutoIndent

        Me.Text = "Bashing - Vinari Software v" & Application.ProductVersion
        GuardarScriptToolMenu.Enabled = False

        FastColoredTextBox1.ForeColor = My.Settings.FontColorFCTBScript
        FastColoredTextBox1.BackColor = My.Settings.BackgroundFCTBScript
        FastColoredTextBox1.IndentBackColor = My.Settings.IndentColorFCTBScript
        FastColoredTextBox1.LineNumberColor = My.Settings.LineNumberColorFCTBScript
        FastColoredTextBox1.CurrentLineColor = My.Settings.CurrentLineColorFCTBScript


        MenuStrip.BackColor = My.Settings.BackgroundMenuStripScript
        MenuStrip.ForeColor = My.Settings.FontColorMenuStripScript

        ArchivoToolStripMenuItem.ForeColor = My.Settings.FontColorMenuStripScript
        EditarToolStripMenuItem.ForeColor = My.Settings.FontColorMenuStripScript
        VerToolStripMenuItem.ForeColor = My.Settings.FontColorMenuStripScript
        LenguajeToolStripMenuItem.ForeColor = My.Settings.FontColorMenuStripScript
        AcercaDeToolStripMenuItem.ForeColor = My.Settings.FontColorMenuStripScript

        Me.WindowState = My.Settings.ScriptWindowState
        Me.Location = My.Settings.ScriptLocation
    End Sub

    Private Sub GuardarScriptToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles GuardarScriptToolMenu.Click

        If SavedFile <> "" Then
            Try
                My.Computer.FileSystem.WriteAllText(SavedFile, FastColoredTextBox1.Text, False)
            Catch ex As Exception
                MsgBox("Se produjo un error al guardar el archivo. ¿Tiene permiso de escritura?" & vbNewLine & "ERROR: 001A", MessageBoxIcon.Error)
            End Try
            Return
        ElseIf OpenFileDialog1.FileName <> "" Then
            Try
                My.Computer.FileSystem.WriteAllText(OpenFileDialog1.FileName, FastColoredTextBox1.Text, False)
            Catch ex As Exception
                MsgBox("Se produjo un error al guardar el archivo. ¿Tiene permiso de escritura?" & vbNewLine & "ERROR: 001A", MessageBoxIcon.Error)
            End Try
            Return
        End If
    End Sub

    Private Sub ModoDeSoloLecturaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SoloLecturaToolMenu.Click
        If SoloLecturaToolMenu.Checked = False Then
            FastColoredTextBox1.ReadOnly = True
            SoloLecturaToolMenu.Checked = True
        Else
            FastColoredTextBox1.ReadOnly = False
            SoloLecturaToolMenu.Checked = False
        End If
    End Sub

    Private Sub CcppToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CppLanguage.Click
        FastColoredTextBox1.Language = FastColoredTextBoxNS.Language.CSharp
        SaveFileDialog1.Filter = "Archivo de C++ |*.cpp|Archivo de C++ |*.cxx|Cabecera de C++ |*.h"

        BashLanguage.Checked = False
        CommandLanguage.Checked = False
        PythonLanguage.Checked = False
        RubyLanguage.Checked = False
        JavaLanguage.Checked = False
        JSLanguage.Checked = False
        CLanguage.Checked = False
        ObjectiveCLanguage.Checked = False
        PHPLanguage.Checked = False
        CSSLanguage.Checked = False
        HTMLLanguage.Checked = False
        CSharpLanguage.Checked = False
        ValaLanguage.Checked = False
        LuaLanguage.Checked = False
        SQLLanguage.Checked = False

        CppLanguage.Checked = True

        FastColoredTextBox1.OnTextChanged()
    End Sub

    Private Sub JavajavaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles JavaLanguage.Click
        FastColoredTextBox1.Language = FastColoredTextBoxNS.Language.CSharp
        SaveFileDialog1.Filter = "Archivo de Java |*.java"

        BashLanguage.Checked = False
        CommandLanguage.Checked = False
        PythonLanguage.Checked = False
        RubyLanguage.Checked = False
        JSLanguage.Checked = False
        CppLanguage.Checked = False
        CLanguage.Checked = False
        ObjectiveCLanguage.Checked = False
        PHPLanguage.Checked = False
        CSSLanguage.Checked = False
        HTMLLanguage.Checked = False
        CSharpLanguage.Checked = False
        ValaLanguage.Checked = False
        LuaLanguage.Checked = False
        SQLLanguage.Checked = False

        JavaLanguage.Checked = True

        FastColoredTextBox1.OnTextChanged()
    End Sub

    Private Sub BashshToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BashLanguage.Click
        FastColoredTextBox1.Language = FastColoredTextBoxNS.Language.PHP
        SaveFileDialog1.Filter = "Script Bash |*.sh"

        CommandLanguage.Checked = False
        PythonLanguage.Checked = False
        RubyLanguage.Checked = False
        JavaLanguage.Checked = False
        JSLanguage.Checked = False
        CppLanguage.Checked = False
        CLanguage.Checked = False
        ObjectiveCLanguage.Checked = False
        PHPLanguage.Checked = False
        CSSLanguage.Checked = False
        HTMLLanguage.Checked = False
        CSharpLanguage.Checked = False
        ValaLanguage.Checked = False
        LuaLanguage.Checked = False
        SQLLanguage.Checked = False

        BashLanguage.Checked = True

        FastColoredTextBox1.OnTextChanged()
    End Sub

    Private Sub ObjectiveCToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ObjectiveCLanguage.Click
        FastColoredTextBox1.Language = FastColoredTextBoxNS.Language.CSharp
        SaveFileDialog1.Filter = "Archivo de Objective-C |*.m"

        BashLanguage.Checked = False
        CommandLanguage.Checked = False
        PythonLanguage.Checked = False
        RubyLanguage.Checked = False
        JavaLanguage.Checked = False
        JSLanguage.Checked = False
        CppLanguage.Checked = False
        CLanguage.Checked = False
        PHPLanguage.Checked = False
        CSSLanguage.Checked = False
        HTMLLanguage.Checked = False
        CSharpLanguage.Checked = False
        ValaLanguage.Checked = False
        LuaLanguage.Checked = False
        SQLLanguage.Checked = False

        ObjectiveCLanguage.Checked = True

        FastColoredTextBox1.OnTextChanged()
    End Sub

    Private Sub JavaScriptToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles JSLanguage.Click
        FastColoredTextBox1.Language = FastColoredTextBoxNS.Language.JS
        SaveFileDialog1.Filter = "Archivo tipo JavaScript |*.js"

        BashLanguage.Checked = False
        CommandLanguage.Checked = False
        PythonLanguage.Checked = False
        RubyLanguage.Checked = False
        JavaLanguage.Checked = False
        CppLanguage.Checked = False
        CLanguage.Checked = False
        ObjectiveCLanguage.Checked = False
        PHPLanguage.Checked = False
        CSSLanguage.Checked = False
        HTMLLanguage.Checked = False
        CSharpLanguage.Checked = False
        ValaLanguage.Checked = False
        LuaLanguage.Checked = False
        SQLLanguage.Checked = False

        JSLanguage.Checked = True

        FastColoredTextBox1.OnTextChanged()
    End Sub

    Private Sub ValaLanguage_Click(sender As Object, e As EventArgs) Handles ValaLanguage.Click
        FastColoredTextBox1.Language = FastColoredTextBoxNS.Language.CSharp
        SaveFileDialog1.Filter = "Archivo tipo Vala |*.vala"

        BashLanguage.Checked = False
        CommandLanguage.Checked = False
        PythonLanguage.Checked = False
        RubyLanguage.Checked = False
        JavaLanguage.Checked = False
        JSLanguage.Checked = False
        CppLanguage.Checked = False
        CLanguage.Checked = False
        ObjectiveCLanguage.Checked = False
        PHPLanguage.Checked = False
        CSSLanguage.Checked = False
        HTMLLanguage.Checked = False
        CSharpLanguage.Checked = False
        LuaLanguage.Checked = False
        SQLLanguage.Checked = False

        ValaLanguage.Checked = True

        FastColoredTextBox1.OnTextChanged()
    End Sub

    Private Sub CToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CLanguage.Click
        FastColoredTextBox1.Language = FastColoredTextBoxNS.Language.CSharp
        SaveFileDialog1.Filter = "Archivo de C |*.c"

        BashLanguage.Checked = False
        CommandLanguage.Checked = False
        PythonLanguage.Checked = False
        RubyLanguage.Checked = False
        JavaLanguage.Checked = False
        JSLanguage.Checked = False
        CppLanguage.Checked = False
        ObjectiveCLanguage.Checked = False
        PHPLanguage.Checked = False
        CSSLanguage.Checked = False
        HTMLLanguage.Checked = False
        CSharpLanguage.Checked = False
        ValaLanguage.Checked = False
        LuaLanguage.Checked = False
        SQLLanguage.Checked = False

        CLanguage.Checked = True

        FastColoredTextBox1.OnTextChanged()
    End Sub

    Private Sub PythonToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PythonLanguage.Click
        FastColoredTextBox1.Language = FastColoredTextBoxNS.Language.Lua
        SaveFileDialog1.Filter = "Archivo tipo Phyton |*.py"

        BashLanguage.Checked = False
        CommandLanguage.Checked = False
        RubyLanguage.Checked = False
        JavaLanguage.Checked = False
        JSLanguage.Checked = False
        CppLanguage.Checked = False
        CLanguage.Checked = False
        ObjectiveCLanguage.Checked = False
        PHPLanguage.Checked = False
        CSSLanguage.Checked = False
        HTMLLanguage.Checked = False
        CSharpLanguage.Checked = False
        ValaLanguage.Checked = False
        LuaLanguage.Checked = False
        SQLLanguage.Checked = False

        PythonLanguage.Checked = True

        FastColoredTextBox1.OnTextChanged()
    End Sub

    Private Sub CommandcommandToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CommandLanguage.Click
        FastColoredTextBox1.Language = FastColoredTextBoxNS.Language.PHP
        SaveFileDialog1.Filter = "Archivo Command |*.command"

        BashLanguage.Checked = False
        PythonLanguage.Checked = False
        RubyLanguage.Checked = False
        JavaLanguage.Checked = False
        JSLanguage.Checked = False
        CppLanguage.Checked = False
        CLanguage.Checked = False
        ObjectiveCLanguage.Checked = False
        PHPLanguage.Checked = False
        CSSLanguage.Checked = False
        HTMLLanguage.Checked = False
        CSharpLanguage.Checked = False
        ValaLanguage.Checked = False
        LuaLanguage.Checked = False
        SQLLanguage.Checked = False

        CommandLanguage.Checked = True

        FastColoredTextBox1.OnTextChanged()
    End Sub
    Private Sub RubyToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RubyLanguage.Click
        FastColoredTextBox1.Language = FastColoredTextBoxNS.Language.Lua
        SaveFileDialog1.Filter = "Archivo tipo Ruby |*.rb"

        BashLanguage.Checked = False
        CommandLanguage.Checked = False
        PythonLanguage.Checked = False
        JavaLanguage.Checked = False
        JSLanguage.Checked = False
        CppLanguage.Checked = False
        CLanguage.Checked = False
        ObjectiveCLanguage.Checked = False
        PHPLanguage.Checked = False
        CSSLanguage.Checked = False
        HTMLLanguage.Checked = False
        CSharpLanguage.Checked = False
        ValaLanguage.Checked = False
        LuaLanguage.Checked = False
        SQLLanguage.Checked = False

        RubyLanguage.Checked = True

        FastColoredTextBox1.OnTextChanged()
    End Sub

    Private Sub PHPToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PHPLanguage.Click
        FastColoredTextBox1.Language = FastColoredTextBoxNS.Language.PHP
        SaveFileDialog1.Filter = "Archivo tipo PHP |*.php"

        BashLanguage.Checked = False
        CommandLanguage.Checked = False
        PythonLanguage.Checked = False
        RubyLanguage.Checked = False
        JavaLanguage.Checked = False
        CppLanguage.Checked = False
        CLanguage.Checked = False
        ObjectiveCLanguage.Checked = False
        JSLanguage.Checked = False
        CSSLanguage.Checked = False
        HTMLLanguage.Checked = False
        CSharpLanguage.Checked = False
        ValaLanguage.Checked = False
        LuaLanguage.Checked = False
        SQLLanguage.Checked = False

        PHPLanguage.Checked = True

        FastColoredTextBox1.OnTextChanged()
    End Sub

    Private Sub CSSToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CSSLanguage.Click
        FastColoredTextBox1.Language = FastColoredTextBoxNS.Language.Lua
        SaveFileDialog1.Filter = "Archivo de hoja de estilo |*.css"

        BashLanguage.Checked = False
        CommandLanguage.Checked = False
        PythonLanguage.Checked = False
        RubyLanguage.Checked = False
        JavaLanguage.Checked = False
        CppLanguage.Checked = False
        CLanguage.Checked = False
        ObjectiveCLanguage.Checked = False
        PHPLanguage.Checked = False
        JSLanguage.Checked = False
        HTMLLanguage.Checked = False
        CSharpLanguage.Checked = False
        ValaLanguage.Checked = False
        LuaLanguage.Checked = False
        SQLLanguage.Checked = False

        CSSLanguage.Checked = True

        FastColoredTextBox1.OnTextChanged()
    End Sub

    Private Sub HTMLToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HTMLLanguage.Click
        FastColoredTextBox1.Language = FastColoredTextBoxNS.Language.HTML
        SaveFileDialog1.Filter = "Página HTML |*.html"

        BashLanguage.Checked = False
        CommandLanguage.Checked = False
        PythonLanguage.Checked = False
        RubyLanguage.Checked = False
        JavaLanguage.Checked = False
        CppLanguage.Checked = False
        CLanguage.Checked = False
        ObjectiveCLanguage.Checked = False
        PHPLanguage.Checked = False
        CSSLanguage.Checked = False
        JSLanguage.Checked = False
        CSharpLanguage.Checked = False
        ValaLanguage.Checked = False
        LuaLanguage.Checked = False
        SQLLanguage.Checked = False

        HTMLLanguage.Checked = True

        FastColoredTextBox1.OnTextChanged()
    End Sub

    Private Sub CSharpToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CSharpLanguage.Click
        FastColoredTextBox1.Language = FastColoredTextBoxNS.Language.CSharp
        SaveFileDialog1.Filter = "Archivo tipo C# |*.cs"

        BashLanguage.Checked = False
        CommandLanguage.Checked = False
        PythonLanguage.Checked = False
        RubyLanguage.Checked = False
        JavaLanguage.Checked = False
        CppLanguage.Checked = False
        CLanguage.Checked = False
        ObjectiveCLanguage.Checked = False
        PHPLanguage.Checked = False
        CSSLanguage.Checked = False
        JSLanguage.Checked = False
        ValaLanguage.Checked = False
        LuaLanguage.Checked = False
        SQLLanguage.Checked = False

        CSharpLanguage.Checked = True

        FastColoredTextBox1.OnTextChanged()

    End Sub
    Private Sub LuaLanguage_Click(sender As Object, e As EventArgs) Handles LuaLanguage.Click
        FastColoredTextBox1.Language = FastColoredTextBoxNS.Language.Lua
        SaveFileDialog1.Filter = "Archivo tipo Lua |*.lua"

        BashLanguage.Checked = False
        CommandLanguage.Checked = False
        PythonLanguage.Checked = False
        RubyLanguage.Checked = False
        JavaLanguage.Checked = False
        CppLanguage.Checked = False
        CLanguage.Checked = False
        ObjectiveCLanguage.Checked = False
        PHPLanguage.Checked = False
        JSLanguage.Checked = False
        HTMLLanguage.Checked = False
        CSharpLanguage.Checked = False
        ValaLanguage.Checked = False
        CSSLanguage.Checked = False
        SQLLanguage.Checked = False

        LuaLanguage.Checked = True

        FastColoredTextBox1.OnTextChanged()
    End Sub

    Private Sub SQLLanguage_Click(sender As Object, e As EventArgs) Handles SQLLanguage.Click
        FastColoredTextBox1.Language = FastColoredTextBoxNS.Language.SQL
        SaveFileDialog1.Filter = "Archivo tipo SQL |*.sql"

        BashLanguage.Checked = False
        CommandLanguage.Checked = False
        PythonLanguage.Checked = False
        RubyLanguage.Checked = False
        JavaLanguage.Checked = False
        CppLanguage.Checked = False
        CLanguage.Checked = False
        ObjectiveCLanguage.Checked = False
        PHPLanguage.Checked = False
        JSLanguage.Checked = False
        HTMLLanguage.Checked = False
        CSharpLanguage.Checked = False
        ValaLanguage.Checked = False
        CSSLanguage.Checked = False
        LuaLanguage.Checked = False

        SQLLanguage.Checked = True

        FastColoredTextBox1.OnTextChanged()
    End Sub

    Private Sub RustToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RustLanguage.Click
        FastColoredTextBox1.Language = FastColoredTextBoxNS.Language.CSharp
        SaveFileDialog1.Filter = "Archivo tipo Rust |*.rs"

        BashLanguage.Checked = False
        CommandLanguage.Checked = False
        PythonLanguage.Checked = False
        RubyLanguage.Checked = False
        JavaLanguage.Checked = False
        CppLanguage.Checked = False
        CLanguage.Checked = False
        ObjectiveCLanguage.Checked = False
        PHPLanguage.Checked = False
        JSLanguage.Checked = False
        HTMLLanguage.Checked = False
        CSharpLanguage.Checked = False
        ValaLanguage.Checked = False
        CSSLanguage.Checked = False
        LuaLanguage.Checked = False
        SQLLanguage.Checked = False

        RustLanguage.Checked = True

        FastColoredTextBox1.OnTextChanged()
    End Sub

    Function restartFCTB()
        FastColoredTextBox1.Language = FastColoredTextBoxNS.Language.Custom
        SaveFileDialog1.Filter = "Script Bash |*.sh|Archivo Command |*.command|Archivo de Java |*.java|Archivo tipo Lua|*.lua|Archivo de Vala |*.vala|Archivo de Rust |*.rs|Archivo de C++ |*.cpp; *.cxx; *.h|Archivo tipo Objective-C |*.m; *.mm|Archivo tipo C#|*.cs|Archivo tipo C |*.c|Archivo tipo Ruby |*.rb|Archivo tipo Phyton |*.py|Archivo tipo JavaScript |*.js|Archivo tipo SQL|*.sql|Archivo tipo PHP |*.php|Archivo de hoja de estilo |*.css|Página HTML |*.html; *hta; *.htm|Documento de Texto Plano |*.txt"

        BashLanguage.Checked = False
        CommandLanguage.Checked = False
        PythonLanguage.Checked = False
        RubyLanguage.Checked = False
        JavaLanguage.Checked = False
        CppLanguage.Checked = False
        CLanguage.Checked = False
        ObjectiveCLanguage.Checked = False
        PHPLanguage.Checked = False
        JSLanguage.Checked = False
        HTMLLanguage.Checked = False
        CSharpLanguage.Checked = False
        ValaLanguage.Checked = False
        CSSLanguage.Checked = False
        LuaLanguage.Checked = False
        SQLLanguage.Checked = False

        FastColoredTextBox1.OnTextChanged()

        Return Nothing
    End Function

    Private Sub AcercaDeVinariSoftwareToolStripMenuItem_Click_1(sender As Object, e As EventArgs) Handles VinariSoftwareAbout.Click
        Dim result As DialogResult = MessageBox.Show("¿Está seguro que desea visitar el sitio web de Vinari Software?", "Bashing - Vinari Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = MsgBoxResult.Yes Then
            Process.Start("https://vinarisoftware.wixsite.com/vinari")
        End If
    End Sub

    Private Sub AcercaDeVinariOSToolStripMenuItem_Click_1(sender As Object, e As EventArgs) Handles VinariOSAbout.Click
        Dim result As DialogResult = MessageBox.Show("¿Está seguro que desea visitar el sitio web de Vinari OS?", "Bashing - Vinari Software", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        If result = MsgBoxResult.Yes Then
            Process.Start("http://vinarios.me")
        End If
    End Sub

    Private Sub CrearNuevaInstanciaDeEditorDeScriptsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NuevaInstanciaToolMenu.Click
        Dim NewScriptForm As New Script
        NewScriptForm.Show()
    End Sub

    Private Sub CrearNuevaInstanciaDeEditorDeScriptsToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles NuevaInstanciaContext.Click
        NuevaInstanciaToolMenu.PerformClick()
    End Sub

    Private Sub PreferenciasToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles PreferenciasToolMenu.Click
        Preferencias.Show()
    End Sub

    Private Sub CortarLineaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CortarLineaToolMenu.Click
        If CortarLineaToolMenu.Checked = True Then
            CortarLineaToolMenu.Checked = False
            FastColoredTextBox1.WordWrap = False
        Else
            CortarLineaToolMenu.Checked = True
            FastColoredTextBox1.WordWrap = True
        End If
    End Sub

    Private Sub CambiarEstiloDeFuenteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CambiarEstiloDeFuenteToolStripMenuItem.Click
        Dim FontDialog As New FontDialog
        If FontDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
            FastColoredTextBox1.Font = FontDialog.Font
        End If
    End Sub

    Private Sub AumentarFuenteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AumentarFuenteMenuItem.Click
        FastColoredTextBox1.ChangeFontSize(+1)
    End Sub

    Private Sub DisminuirFuenteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DisminuirFuenteMenuItem.Click
        FastColoredTextBox1.ChangeFontSize(-1)
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Application.Exit()
    End Sub

    Private Sub CambiosEnEstaVersiónDeBashingToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CambiosEnEstaVersiónDeBashingToolStripMenuItem.Click
        Novedades.Show()
    End Sub

    Private Sub ColorDeLaFuenteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ColorDeLaFuenteToolStripMenuItem.Click
        ColorDialog1.ShowDialog()
        Dim selectedColor As Color = ColorDialog1.Color()
        FastColoredTextBox1.ForeColor = selectedColor
    End Sub
End Class