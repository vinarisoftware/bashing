﻿Public Class Preferencias
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Try
            My.Settings.TABSize = Convert.ToByte(TextBox1.Text)

            If My.Settings.TABSize < 2 Then
                My.Settings.TABSize = 2
            ElseIf My.Settings.TABSize > 8 Then
                My.Settings.TABSize = 8
            End If

        Catch ex As Exception
            If My.Settings.Language = "en" Then
                MsgBox("The size of TAB can only accept numbers.", MessageBoxIcon.Warning)
            Else
                MsgBox("Solamente se aceptan valores numéricos para la longitud de TAB.", MessageBoxIcon.Warning)
            End If
        End Try

        If CheckBox1.Checked = True Then
            My.Settings.AutoCompleteBrackets = True
        Else
            My.Settings.AutoCompleteBrackets = False
        End If

        If CheckBox2.Checked = True Then
            My.Settings.AutoIndent = True
        Else
            My.Settings.AutoIndent = False
        End If

        My.Settings.Save()

        If My.Settings.Language = "en" Then
            MsgBox("You will need to restart Bashing - Vinari Software if you need all changes to take efect.", MessageBoxIcon.Information)
        Else
            MsgBox("Para que algunos cambios aplicados se apliquen, se necesita reiniciar Bashing - Vinari Software.", MessageBoxIcon.Information)
        End If

        Me.Close()

    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Script.SolarOscuroTheme.PerformClick()
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        Script.ClassicTheme.PerformClick()
    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Script.MoonTheme.PerformClick()
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        Script.WarmCoffeeTheme.PerformClick()
    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click
        Script.KingdomTheme.PerformClick()
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        Script.AuroraTheme.PerformClick()
    End Sub
    Private Sub Button22_Click(sender As Object, e As EventArgs) Handles Button12.Click
        Script.CalmOceanTheme.PerformClick()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Script.GrapeTheme.PerformClick()
    End Sub

    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click
        Script.BlackBoardTheme.PerformClick()
    End Sub

    Private Sub Button14_Click(sender As Object, e As EventArgs) Handles Button14.Click
        Script.SolarClaroTheme.PerformClick()
    End Sub
    Private Sub Button15_Click(sender As Object, e As EventArgs) Handles Button15.Click
        Script.DraculaTheme.PerformClick()
    End Sub

    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button3.Click
        My.Settings.Reset()
        My.Settings.Save()
    End Sub

    Private Sub Preferencias_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If My.Settings.AutoCompleteBrackets = True Then
            CheckBox1.Checked = True
        Else
            CheckBox1.Checked = False
        End If

        If My.Settings.AutoIndent = True Then
            CheckBox2.Checked = True
        Else
            CheckBox2.Checked = False
        End If

        TextBox1.Text = Byte.Parse(My.Settings.TABSize)

        If My.Settings.Language = "es" Then
            RadioButton5.Checked = True
        ElseIf My.Settings.Language = "en" Then
            RadioButton6.Checked = True

            Me.Text = "Bashing - Vinari Software | Settings"
            Label1.Text = "Bashing | Settings."
            Label2.Text = "Spaces per TAB pressed:"

            CheckBox1.Text = "Autocomplete parenthesis."
            CheckBox2.Text = "Autoindent text."

            GroupBox1.Text = "Window state"
            GroupBox2.Text = "Editor themes"
            GroupBox4.Text = "Language (BETA)"

            Button6.Text = "Default"
            Button2.Text = "Start maximized."
            Button5.Text = "Start at normal size."

            Button1.Text = "Apply changes and close this window"
            Button3.Text = "Revert all to default"

        End If
    End Sub

    Private Sub RadioButton5_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton5.CheckedChanged
        My.Settings.Language = "es"
    End Sub

    Private Sub RadioButton6_CheckedChanged(sender As Object, e As EventArgs) Handles RadioButton6.CheckedChanged
        My.Settings.Language = "en"
    End Sub

    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
        RadioButton5.PerformClick()
    End Sub

    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBox4.Click
        RadioButton6.PerformClick()
    End Sub

    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        My.Settings.ScriptWindowState = FormWindowState.Maximized
    End Sub

    Private Sub Button5_Click_1(sender As Object, e As EventArgs) Handles Button5.Click
        My.Settings.ScriptWindowState = FormWindowState.Normal
    End Sub
End Class