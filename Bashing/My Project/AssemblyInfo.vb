﻿Imports System.Reflection
Imports System.Resources
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Bashing - Vinari Software")>
<Assembly: AssemblyDescription("¡El Editor Simple!")>
<Assembly: AssemblyCompany("Vinari Software")>
<Assembly: AssemblyProduct("Bashing - Vinari Software")>
<Assembly: AssemblyCopyright("Copyright: BSD License Vinari Software © 2015 - 2023")>
<Assembly: AssemblyTrademark("Vinari Software © 2015 - 2023")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("e78a7d05-b365-495e-a2d6-9f79cb88c4ba")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("0.9.7.1")>
<Assembly: AssemblyFileVersion("0.9.7.1")>
<Assembly: NeutralResourcesLanguage("es")>
